+++
title = "Persona Aliye"
weight = 30

[asset]
	image = "aliye.png"
	title = "Aliyes Steckbrief"

[[requirements]]
	text = "Aliye möchte die App in **jedem Bezirk** mit den **gleichen Funktionen** und **gleichem Design** vorfinden, damit sie sich nicht neu in die Handhabung der App einarbeiten muss."

[[requirements]]
	text = "Aliye möchte sich über die App **mit** anderen **Kiezbewohner:innen vernetzen** können, die sich für die gleichen Themen interessieren."

[[requirements]]
	text = "Aliye möchte **möglichst viele Informationen aus ihrer Umgebung** angezeigt bekommen und diese nach Schlagwörtern filtern können, um relevante Informationen schnell aufzufinden. "

[[requirements]]
	text = "Aliye möchte sich über **Hilfsangebote und Initiativen** im Kiez informieren, um sich bei Bedarf Unterstützung zu suchen und zu vernetzen."

[[requirements]]
	text = "Aliye möchte mit Hilfe der App **direkt** mit der zuständigen **Verwaltungsstelle in Kontakt treten** können, um unkompliziert ihre Anliegen zu besprechen."

[[requirements]]
	text = "Aliye möchte die App in ihrer **Muttersprache** und **leichter Sprache** nutzen können, um schnell ein gutes Verständnis über Inhalte zu entwickeln."

[[requirements]]
	text = "Aliye möchte direkt in der App zwischen **mehreren Lösungsvorschlägen** wählen können, um konkrete Ideen nicht erst selbst entwickeln zu müssen."

[[requirements]]
	text = "Aliye möchte für die mögliche **Komplexität von Beteiligungsprojekten** sensibilisiert werden, um falsche Erwartungen zu vermeiden."
+++

- Sie ist
	- aktiv in der Nachbarschaft
	- nicht sonderlich technikaffin
  - die mit den guten Ratschlägen
- Sie hat
	- einen großen Freundes- und Bekanntenkreis
	- kein Interesse an komplizierten Details zu politischen Verfahren
- Sie strebt
	- nach einem guten Leben für sich und ihre Familie
	- nach Erhalt des Kiezlebens in bisheriger Form
- Sie fordert
	- geringe technische Hürden bei der Beteiligung
	- Mitsprache bei Entscheidungen, die ihr Leben im Kiez verändern
