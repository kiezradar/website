+++
fragment = "personas"
weight = 20

title = "Personas"
+++

Personas helfen, die Entwicklungen einer App nicht nur aus der Perspektive einer Projektleiterin oder eines technischen Entwicklers zu betrachten, sondern die Sicht verschiedener Interessens- und Nutzergruppen einzunehmen.
Dadurch lassen sich wichtige Anforderungen und Bedürfnisse ableiten, wie bspw. Mehrsprachigkeit oder barrierefreies Design.
Die Erstellung von Personas fördert daher eine nutzerzentrierte Entwicklung, auch weil sie helfen, die Motive, Erfahrungen, Verhaltensweisen und Ziele der Nutzer:innen besser zu verstehen.

Unsere Personas enstanden im Nachgang an den Bürgerworkshop im November 2019.
Die ausgewählten Personas sollen die drei Nutzergruppen repräsentieren, die wir als Projekt-Team für die KiezRadar-App als besonders relevant eingestuft haben:

- **Technik-affine Nutzer:innen**
- **Politisch vernetzte Berliner:innen**
- **Alteingessene Kiezbewohner:innen**

Die Personas wurden auf Basis von Recherchen und mehrerer Interviews mit Vertreter:innen der jeweiligen Nutzergruppe erstellt.
Die **Anforderungen** der Personas stammen entweder aus dem Bürgerworkshop oder konnten aus den Interviews mit den Repräsentant:innen der Nutzergruppen abgeleitet werden.
