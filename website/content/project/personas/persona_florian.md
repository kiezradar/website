+++
title = "Persona Florian"
weight = 20

[asset]
	image = "florian.png"
	title = "Florians Steckbrief"

[[requirements]]
	text = "Florian möchte, dass ein **achtsamer Umgang** gefördert wird und **Manipulationen** von Beteiligungen vorgebeugt werden, um Konflikte und Frust zu vermeiden."

[[requirements]]
	text = "Florian möchten Projekte abonnieren können und über **Veränderungen, Projektfortschritte oder Ergebnisse** informiert werden, um die gesamte Projektentwicklung mitzubekommen."

[[requirements]]
	text = "Florian möchte wissen, welche Beteiligungen bei anderen **Kiezbewohner:innen beliebt** sind, um spannende Beteiligungen möglichst einfach zu entdecken."

[[requirements]]
	text = "Florian möchte, dass seine Meinung von der Verwaltung in Form von **direkten Bürgerbefragungen** eingeholt wird, um ohne viel Aufwand seine Meinung abgeben zu können."

[[requirements]]
	text = "Florian möchte, dass Beiträge **langfristig und in hoher Qualität** veröffentlicht werden, um zu einer steten Nutzung der App motiviert zu werden."

[[requirements]]
	text = "Florian möchte auf möglichst **aktuelle** Daten und Informationen zugreifen, um Frust zu vermeiden und einen Anreiz für eine stete Nutzung zu haben."

[[requirements]]
	text = "Florian möchte informiert werden, wenn er an einer **Beteiligungsmöglichkeit vorbeifährt**, um diese mit der Umgebung zu verbinden."

[[requirements]]
	text = "Florian möchte **voll-umfassend über den geplanten Ablauf, die Ziele und die Verwertung der Ergebnisse** der Beteiligungsverfahren informiert werden, da Transparenz für Interesse und Vertrauen sorgt."
+++

- Er ist
	- engagiert, doch etwas unorganisiert
	- der ausgleichende Vermittler im Freundeskreis
  - mit digitalen Medien aufgewachsen
- Er hat
	- viele Interessen und ein großes Verantwortungsgefühl
	- ein überlegtes App Nutzungsverhalten
- Er strebt
	- nach sinnvoller Freizeitbeschäftigung, die Spaß bringt
	- nach Kontakt zu neuen Menschen
- Er fordert
	- eine übersichtliche, bequeme Information über Beteiligungen
	- eine Mischung aus digitaler und analoger Beteiligung
