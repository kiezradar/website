+++
title = "Persona Nora"
weight = 10

[asset]
	image = "nora.png"
	title = "Noras Steckbrief"

[[requirements]]
	text = "Nora möchte, dass die KiezRadar-App über die reine Information zu Beteiligungen hinausgeht und **schwer zu erreichenden Information zusammenführt**, damit sie möglichst viel mitbekommt."

[[requirements]]
	text = "Nora möchte durch die App **konkrete Vorschläge** zu bestimmten Themen einreichen können."

[[requirements]]
	text = "Nora möchte die App als **Open Source-Software** transparent einsehen können, da sie Wert auf die Sicherheit und Transparenz ihrer Anwendung legt."

[[requirements]]
	text = "Nora möchte, dass die App eine **Vielzahl an Medien und Inhalten** von anderen Plattformen einbindet, damit sie eine möglichst **breite Informationsbasis** zu Beteiligungsangeboten erhält."

[[requirements]]
	text = "Nora möchte **eigene Initiativen und Beteiligungsformate** gründen und anzeigen, um die Bürger:innen zu vernetzen und ihre Interessen abzubilden."

[[requirements]]
	text = "Nora möchte ausführlich darüber informiert werden, **wann** (bei welchen Funktionen), **zu welchen Zweck, von wem und warum, welche Daten erhoben werden**, um der App zu vertrauen."

[[requirements]]
	text = "Nora möchte **anonym** an Beteiligungen teilnehmen, um keine Daten weitergeben zu müssen."

[[requirements]]
	text = "Nora wünscht sich eine **Offenlegung** der bei der KiezRadar-App genutzten **Daten**, um sie auch für andere Projekte in ihrem Kiez nutzen zu können."
+++

- Sie ist
	- gut vernetzt und informiert
	- die kritische Stimme im Freundeskreis
	- offen und neugierig auf neue Konzepte
- Sie hat
	- ein hohes Informationsbedürfnis
	- fortgeschrittene Programmierkenntnisse
	- hohe Ansprüche an genutzte Apps
- Sie strebt
	- nach Unabhängigkeit von großen Konzernen
	- danach Wissen und Fähigkeiten zu teilen
- Sie fordert
	- möglichst transparente, digitale und effiziente Beteiligungsformate
	- ein möglichst hohes Maß an Datensouveränität und –sicherheit
