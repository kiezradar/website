+++
fragment = "content"
title = "Das Projekt"
title_align = "left"
date = "2020-10-13"
weight = 10
+++

### Ziele des Projekts
#### Verbesserte Information als Schlüssel zu mehr Bürgerbeteiligung

Was passiert in meinem Kiez? Welche Beteiligungsmöglichkeiten gibt es in meiner Umgebung? Womit befasst sich die Lokalpolitik? Statt sich selbst durch die digitalen Angebote Berlins zu wühlen, soll die KiezRadar-App Bürger:innen zukünftig proaktiv und bedarfsgerecht über wichtige Ereignisse aus Politik und Verwaltung informieren. Beispielsweise sollen die Nutzer:innen die Themengebiete oder den Umkreis für den ihnen Beteiligungen angezeigt werden selbst wählen können. Dabei sollen die Informationen der bisherigen Informationsangebote wie bspw. mein.Berlin oder das Ratsinformationssystem genutzt und gezielt für die Nutzer:innen aufbereitet werden.

#### App-Entwicklung als partizipativer Prozess

Zusammen mit dem neuen stadteigenen digitalen Experimentierlabor für die Stadt der Zukunft, dem CityLAB Berlin, sollen neue Konzepte der Bürger-Verwaltungs-Kommunikation erprobt werden. Durch die Einbindung der Nutzer:innen in die Entwicklung der App wird das Thema Bürgerbeteiligung bereits bei der Entstehung mitgedacht.

#### Projektvorgehen

Der gesamte Projektzeitraum erstreckt sich vom **2. Oktober 2019 bis 31. März 2021**. Derzeit können wir jedoch noch nicht absehen, in wie weit sich das Projekt aufgrund der aktuellen Corona-Krise verzögert.

Das Projekt KiezRadar teilt sich in vier Phasen, innerhalb derer bestimmte Ergebnisse erzielt werden sollen. Die Phasen bauen grundsätzlich aufeinander auf, werden aufgrund eines agilen Projektansatzes aber auch parallel bearbeitet. Über die Forschritte des Projekts wird regelmäßig in unserem [Blog](/blog) berichtet.

Ziel des Projekts ist es den Zugang zu den digitalen Angeboten Berlins zu vereinfachen. Daher soll die KiezRadar-App die Bürger:innen zukünftig proaktiv und bedarfsgerecht über wichtige Ereignisse aus Politik und Verwaltung informieren. Die vorgeschlagenen Ereignisse sollen dabei anhand der Bedürfnisse und Vorlieben der Bürger:innen ausgewählt werden, um eine Nutzerfreundliche Anwendung zu ermöglichen. Folglich soll die Partizipation und schließlich auch die Interaktion so einfach und intuitiv wie möglich gemacht werden.

<figure class="figure">
  <img src="/images/Zeitstrahl.png" class="figure-img img-fluid rounded" alt="Projektzeitstrahl">
  <figcaption class="figure-caption">KiezRadar Projektzeitstrahl</figcaption>
</figure>
