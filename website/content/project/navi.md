+++
fragment = "content"
weight = 200
disabled = true
+++

## Ziele des Projekts
### Verbesserte Information als Schlüssel zu mehr Bürgerbeteiligung
### App-Entwicklung als partizipativer Prozess
### Projektvorgehen

## Personas
### Persona Nora
### Persona Florian
### Persona Aliye

## Mockups
### Mockup Startseite
### Mockup Entdecken
### Mockup Filter
### Mockup Einstellungen
### Mockup Suche
### Mockup Favoriten
### Mockup Benachrichtigungen
### Mockup Detailansicht

## Anwendungsszenarien
### Beispiel Quartiersmanagement
### Beispiel meinBerlin
### Beispiel Ratsinformationssystem
