+++
title = "Mockup Favoriten"
date = "2020-04-16"
weight = 70

[[assets]]
	image = "Favoriten_Alinks.png"
	title = "Anforderung Favoriten"

[[assets]]
  image = "Favoriten.png"
	title = "Mockup Favoriten"

[[assets]]
  image = "Favoriten_Arechts.png"
	title = "Anforderung Favoriten"
+++

- Welche Ordnung ist hier gewünscht?
	- Soll es bspw. eine Unterscheidung geben zwischen merken/bzw. später lesen und folgen?
	- Sollen Projektfortschritte hier besonders gut aufbereitet werden?
	- Was legt die Reihenfolge der Anzeige fest? (zuletzt hinzugefügt?, letzte Aktivität? Erstellungsdatum?
	- Wird die Suchfunktion benötigt?
