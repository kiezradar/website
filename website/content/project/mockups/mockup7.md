+++
title = "Mockup Benachrichtigungen"
date = "2020-04-16"
weight = 80

[[assets]]
	image = "Benachrichtigungen_Alinks.png"
	title = "Anforderung Benachrichtigungen"

[[assets]]
  image = "Benachrichtigungen.png"
	title = "Mockup Benachrichtigungen"

[[assets]]
  image = "Benachrichtigungen_Arechts.png"
	title = "Anforderung Benachrichtigungen"
+++

- Fragen an unsere Nutzer:innen
	- Soll sich hierfür eine eigene Seite oder eher ein Fenster öffnen?
	- Soll es Icons geben die zwischen Projektfortschritt und neue Beteiligung unterscheiden?
