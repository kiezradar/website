+++
title = "Mockup Entdecken"
date = "2020-04-22"
weight = 30

[[assets]]
	image = "Entdecken.png"
	title = "Mockup Entdecken"

[[assets]]
	image = "Entdecken_Arechts.png"
	title = "Anforderung Entdecken"
+++

- Fragen an unsere Nutzer:innen
	- Wollen sie hier schon die Möglichkeit haben, die Beteiligungen zu bewerten oder zu den Favoriten hinzuzufügen?
	- Ist die Auswahl der in der Zusammenfassung angezeigten Infos gelungen oder würden sie andere Schwerpunkte setzen?
	- Sollen die Bilder größer sein oder sollen gar keine Bilder angezeigt werden in der Zusammenfassung, damit mehr Beteiligungen angezeigt werden können?
	- Ist die Bedeutung der Beschriftung des ersten Reiters mit "Eigene" klar?
	- Wo soll der Button für die Bewertung und für Hinzufügen zu Favoriten platziert werden?
	- Wir hier eine integrierte Suchfunktion benötigt?
	- Ist die Funktionsmöglichkeit für in Karte anzeigen gewünscht?
