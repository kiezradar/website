+++
title = "Mockup Detailansicht"
date = "2020-05-27"
weight = 90

[[assets]]
	image = "Detailbereich_Kiezradar_deutsch_Leiste.png"
	title = "Mockup Datailbereich"

[[assets]]
  image = "Detailbereich_Kiezradar_deutsch_V2_mit_Auswahlleiste.png"
	title = "Mockup Datailbereichauswahlleiste"

+++

- Fragen an unsere Nutzer:innen
	- Soll der Ort der Beteiligung in einer separaten Kartenansicht angezeigt oder direkt in der mapinternen Kartenfunktion geöffnet werden?
