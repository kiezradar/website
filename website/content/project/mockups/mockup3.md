+++
title = "Mockup Filter"
date = "2020-04-22"
weight = 40

[[assets]]
	image = "Filter_V1.png"
	title = "Mockup Filter 1"

[[assets]]
	image = "Filter_Alinks.png"
	title = "Anforderung Filter"

[[assets]]
	image = "Filter_V2.png"
	title = "Mockup Filter 2"

[[assets]]
	image = "Filter_Arechts.png"
	title = "Anforderung Filter"
+++

- Fragen an unsere Nutzer:innen
	- Sind alle notwendigen Filterparameter vorhanden?
	- Welche Art die Entfernung einzustellen wird bevorzugt?
	- Sind die Formen der Filter intuitiv gestaltet?
