+++
fragment = "mockup"
weight = 30

title = "Mockups"
+++

Mockup meint in unserem Falle einen grafischen Entwurf der späteren KiezRadar-Anwendung.
Mockups bedeuten nicht, dass die spätere KiezRadar-Apps zwangläufig exakt so aussiehen muss.
Sie bieten jedoch eine gute Möglichkeit, um verschiedene Designideen zu erproben und zu diskutieren.
Zudem kommen bei der Erstellung viele Fragen zu Präferenzen oder Bedürfnissen der Nutzer:innen auf, die wir mit diesen besprechen können.
Die entsprechenden Fragen können unterhalb der jeweiligen Mockups nachvollzogen werden.

Bei den Designentwürfen wird darauf geachtet, eine Vielzahl der erhobenen Anforderungen aus dem Bürgerworkshops zu berücksichtigen.
Die bereits umgesetzten Anforderungen können anhand der Sprechblasen der Personas nachvollzogen werden.
Zudem achten wir darauf, die Vorgaben der Barrierefreiheit nach WCAG 2.1 einzuhalten, bspw. in Bezug auf den Kontrast von Grafiken und Schriften.

Die Mockups werden suksessive angepasst und verfeinert.
Ihr Fortschreiten wollen wir transparent nachvollziehbar halten.

Dies sind die ersten Versionen der Mockups für 7 Funktionsbereiche.
