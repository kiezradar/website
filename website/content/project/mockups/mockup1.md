+++
title = "Mockup Startseite"
date = "2020-04-22"
weight = 25

[[assets]]
	image = "Startseite.png"
	title = "Mockup Startseite"

[[assets]]
	image = "Startseite-Radar.png"
	title = "Mockup Startseite Radar"
+++

- Fragen an unsere Nutzer:innen
	- Wird eine extra Startansicht gewollt oder würden sie lieber direkt mit der Karte, Suche oder Entdecken-Funktion starten?
	- Welche der 3 Möglichkeiten fände sie die geeigneteste Startseite?
	- Ist es intuitive, dass sich die Kacheln des Filters für auf die Funktion "Entdecken" und die Karte bezieht?
