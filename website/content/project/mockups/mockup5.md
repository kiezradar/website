+++
title = "Mockup Suche"
date = "2020-04-16"
weight = 60

[[assets]]
	image = "Suche.png"
	title = "Mockup Suche"
+++

- Fragen an unsere Nutzer:innen
	- Sollen die Suchergebnisse genauso angezeigt werden wie in Entdecken? Oder in Listenform mit weniger Informationen?
	- Soll die Trefferanzahl angezeigt werden? wird meist nicht mehr gemacht
	- Sollen die Reiter anders aussehen als bei Discover, um mehr der Suchoptik entsprechen?
