+++
fragment = "content"
weight = 40

title = "Anwendungsszenarien"
title_align = "left"
+++

Anwendungsszenarien dienen dazu, die angestrebte Interaktion zwischen den Nutzer:innen und dem System ins Zentrum zu rücken. Dadurch lassen sich die Interaktionsschritte bspw. kritisch betrachten oder veranschaulichen.

### Beispiel Quartiersmanagement

<dl>
	<dt>Akteur:innen</dt>
	<dd>Benutzer:innen (Aliye)</dd>
	<dt>Auslöser</dt>
	<dd>Aliye hat die Kiezradar-App frisch installiert und möchte für sie passende Angebot entdecken.</dd>
	<dt>Normalablauf</dt>
	<dd>
		<ol>
			<li>Aliye startet die KiezRadar-App und klickt auf die Filterfunktion oben rechts in der KiezRadar-App.</li>
			<li>Dort wählt sie für sie passende Themenbereiche aus und setzt den Umkreis für die Anzeige von Einträgen auf einen Radius von 5 km um ihren Wohnort.</li>
			<li>Aliye geht auf "Entdecken" und sieht sich die Einträge unter dem ersten Reiter "Eigene" an.</li>
			<li>Der wöchentlich stattfindende Upcycling-Workshop weckt Aliyes Interesse und sie klickt auf die Veranstaltung für weitere Informationen.</li>
			<li>Aliye liest sich die Beschreibung durch, fügt den nächsten Termin dem Smartphone-eigenen Kalender zu und markiert die Reihe unter Favoriten.</li>
		</ol>
	</dd>
</dl>

### Beispiel meinBerlin

<dl>
	<dt>Akteur:innen</dt>
	<dd>Benutzer:innen (Florian)</dd>
	<dt>Auslöser</dt>
	<dd>Florian erhält durch die KiezRadar-App eine Benachrichtung über die Beteiligungsmöglichkeit "Aktionsfonds 2020 QM Flughafenstraße". Die Beteiligung entspricht seinen eingestellten Präferenzen in Bezug auf das Wohngebiet und das Themengebiet "Stadtentwicklung".</dd>
	<dt>Normalablauf</dt>
	<dd>
		<ol>
			<li>Florian sieht anhand des Benachrichtigungsicons, dass eine neue Benachrichtung vorliegt und lässt sich diese anzeigen.</li>
			<li>Durch das Klicken auf den Infotext zur der Beteiligungsmöglichkeit "Aktionsfonds 2020 QM Flughafenstraße" gelangt Florian zu dem Detailbereich der Beteiligung.</li>
			<li>Im Detailbereich liest sich Florian die zusammenfassende Projektbeschreibung durch.</li>
			<li>Florian klickt auf den Button "Beteiligen" und wird auf die externe Projektwebseite von "Aktionsfonds 2020 QM Flughafenstraße" auf mein.berlin.de weitergeleitet.</li>
			<li>Florian bekommt eine Informationsseite zur Vorschlagsphase angezeigt und klickt auf den Button "Vorschlag anlegen".</li>
			<li>Florian gibt seine Nutzerdaten für mein.berlin.de ein.</li>
			<li>Florian gibt einen Vorschlag für das Beteiligungsverfahren ab: er versieht seinen Vorschlag mit einem Titel, verfasst einen beschreibenden Text, lädt ein Bild hoch, bestätigt den Besitz der Urheberrechte an dem Bild, legt einen Budgetrahmen fest und verortet seinen Vorschlag durch die Angabe einer Straße und Ortbezeichnung.</li>
			<li>Florian schließt den Vorgang durch Klicken auf den Button "Speichern" ab.</li>
			<li>Florian wechselt zurück in die KiezRadar-App und fügt das Projekt zu seinen Favoriten hinzu, um über die weiteren Projektfortschritte informiert zu werden.</li>
		</ol>
	</dd>
	<dt>Alternative Abläufe</dt>
	<dd>
		<ul>
			<li>3.1 Florian liest sich die Projektbeschreibung nicht in der KiezRadar-App durch, sondern auf der Projektseite von meinBerlin.</li>
			<li>5.1 Florian klickt auf den Button Anmelden/Registrieren und wählt bei den ausklappenden Reitern den Schritt "registrieren" aus.</li>
			<li>5.2 In der sich öffnenden Maske gibt Florian seine E-Mail-Adresse ein und wählt einen Benutzernamen und ein Passwort. Nun wählt er aus, dass er keine Benachrichtigungen zu den Beteiligungen über die Mailadresse bekommen möchte und abonniert den Newsletter von meinBerlin. Anschließend willigt er in die Verarbeitung seiner Daten ein und bestätigt durch das Lösen eines Rätsels, dass er kein Roboter ist.</li>
		</ul>
	</dd>
</dl>

### Beispiel Ratsinformationssystem

<dl>
	<dt>Akteur:innen</dt>
	<dd>Benutzer:innen (Nora)</dd>
	<dt>Auslöser</dt>
	<dd>Nora interessiert sich für das Thema Clubkultur in Berlin und hat Sitzungen zu diesem Thema unter Favoriten gespeichert. Sie bekommt eine Benachrichtigung mit dem Datum der nächsten Sitzung angezeigt.</dd>
	<dt>Normalablauf</dt>
	<dd>
		<ol>
			<li>Nora sieht anhand des Benachrichtigungsicons, dass eine neue Benachrichtung vorliegt und lässt sich diese anzeigen.</li>
			<li>Durch das Klicken auf den Text der Benachrichtigung gelangt Nora zu dem Detailbereich der Tagesordnung der Sitzung und findet das Thema Clubkultur in Berlin unter Tagesordnungspunkt 9 "Open-Airs 2020 ermöglichen, geeignete Flächen ausweisen, Genehmigungsverfahren erleichtern, Clubkultur retten!"</li>
			<li>Nora fügt den nächsten Sitzungstermin dem Smartphone-eigenen Kalender hinzu.</li>
		</ol>
	</dd>
</dl>
