+++
fragment = "content"
title = "Berlin Science Week 2020"
title_align = "left"
date = "2020-10-21"
weight = 100
+++

Dieses Jahr findet die Berlin Science Week vom 1.11. bis 10.11.2020 im [CityLAB](https://www.citylab-berlin.org/) statt.

KiezRadar ist dabei und zeigt den ersten Prototypen der App.
Seien Sie dabei, wie sich die App anfühlt und geben Sie uns Feedback.

Die offizielle Ankündigung ist erfolgt:

- [Montag, 9. November 2020, 14 - 17 Uhr](https://falling-walls.com/event/testing-kiezradar/)
- [Dienstag, 10. November 2020, 14 - 17 Uhr](https://falling-walls.com/event/testing-kiezradar-2/)

### Helfen Sie mit, Bürgerbeteiligung per App zu erproben und zu verbessern!

In diesem Jahr findet die Berlin Science Week vom 1. November bis 10. November 2020 im CityLAB Berlin statt.
Dies nehmen wir zum Anlass, dort den ersten Prototypen unserer KiezRadar-App vorzustellen und gemeinsam mit Ihnen, den späteren Nutzer*innen, zu evaluieren:

**Wir laden Sie herzlich ein, den Prototypen der App vor Ort gemeinsam mit uns zu erkunden, Feedback zu geben und Ideen zur Verbesserung einzubringen.**

Dafür vergeben wir Zeitslots in der Länge von ca. 30 Minuten, die von Ihnen gebucht werden können. Melden Sie sich an: Wir sind gespannt auf Ihr Feedback!

Wir sind am 9. und 10. November 2020 jeweils von 14 - 17 Uhr vor Ort, um mit Ihnen gemeinsam die App zu erkunden und Ihre Erfahrungen aufzunehmen.
Wir vergeben dafür Slots von ca. 30 Minuten, die von Ihnen gebucht werden können.

Die Anmeldung erfolgt über die folgende [Anmeldeseite](https://www.fokus.fraunhofer.de/de/dps/events/KiezRadar_testen):

- https://www.fokus.fraunhofer.de/de/dps/events/KiezRadar_testen

### Was ist das Projekt KiezRadar?

Was passiert in meinem Kiez?
Welche Beteiligungsmöglichkeiten gibt es in meiner Umgebung?
Womit befasst sich die Lokalpolitik?
Statt sich selbst durch die digitalen Angebote Berlins zu wühlen, soll die KiezRadar-App Bürger:innen zukünftig proaktiv und bedarfsgerecht über wichtige Ereignisse aus Politik und Verwaltung informieren.
KiezRadar soll als Ergänzung zu vorhandenen Berliner Informationsangebote fungieren und dazu beitragen, die bisherige Reichweite dieser Informationskanäle zu vergrößern.
