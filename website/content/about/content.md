+++
fragment = "content"
title = "Über das Projekt KiezRadar"
title_align = "left"
date = "2020-10-13"
weight = 100
+++

### Verbesserte Information als Schlüssel zu mehr Bürgerbeteiligung

Was passiert in meinem Kiez? Welche Beteiligungsmöglichkeiten gibt es in meiner Umgebung? Womit befasst sich die Lokalpolitik? Statt sich selbst durch die digitalen Angebote Berlins zu wühlen, soll die KiezRadar-App Bürger:innen zukünftig proaktiv und bedarfsgerecht über wichtige Ereignisse aus Politik und Verwaltung informieren. Beispielsweise sollen die Nutzer:innen die Themengebiete oder den Umkreis für den ihnen Beteiligungen angezeigt werden selbst wählen können. Dabei sollen die Informationen der bisherigen Informationsangebote wie bspw. mein. Berlin oder das Ratsinformationssystem genutzt und gezielt für die Nutzer:innen aufbereitet werden.

### App-Entwicklung als partizipativer Prozess

Zusammen mit dem neuen stadteigenen digitalen Experimentierlabor für die Stadt der Zukunft, dem CityLAB Berlin, sollen neue Konzepte der Bürger-Verwaltungs-Kommunikation erprobt werden. Durch die Einbindung der Nutzer:innen in die Entwicklung der App wird das Thema Bürgerbeteiligung bereits bei der Entstehung mitgedacht.

### Fraunhofer FOKUS

[Fraunhofer FOKUS](https://www.fokus.fraunhofer.de/) erforscht und entwickelt die vernetzte Welt – sicher, zuverlässig und vertrauenswürdig. Wir vernetzen alles. 20 Milliarden – auf diese Zahl werden die ver­netzten Geräte in Privathaushalten und Wirtschaft bis zum Jahr 2020 ansteigen. Diese Entwicklung wird die Kommunikation und Interaktion in allen Lebens- und Arbeitsbereichen grundlegend verändern, vom hochautomatisierten Fahren, über neue Möglichkeiten der Unterhaltung bis hin zur Smart City und der Fabrik von morgen. Die Vernetzung soll für mehr Lebensqualität, Nachhaltigkeit und Sicherheit sorgen. Um dies zu erreichen, müssen nicht nur Geräte verbunden sein, sondern (fast) alles: Menschen, Dinge, Systeme, Prozesse und Organisationen.

### Unsere Parner

#### CityLAB

Das [CityLAB](https://www.citylab-berlin.org/) ist ein Experimentierlabor für die Stadt der Zukunft. Ein ständig wachsendes Netzwerk aus Verwaltung, Zivilgesellschaft, Wissenschaft und Startups arbeitet hier gemeinsam an neuen Ideen für ein lebenswerteres Berlin. Das CityLAB vereint Elemente aus Digitalwerkstatt, Co-Working und Veranstaltungsraum zu einem Ort, an dem Partizipation und Innovation zusammengedacht werden. Das CityLAB ist kein fertiges Konzept, sondern selbst ein Experiment, das kontinuierlich weiterentwickelt wird.

#### Senatskanzlei Berlin

Die [Senatskanzlei](https://www.berlin.de/rbmskzl/en/) Berlin ist der Projektförder von KiezRadar.

### Die Idee hinter der KiezRadar Webseite

Wir möchten in dem Projekt KiezRadar so transparent und offen arbeiten wie möglich. Darum wollen wir möglichst viel von unserer Arbeit im Projekt teilen und für Euch nachvollziehbar machen. Egal ob Projektschritte, Zwischenergebnisse, aufkommende Fragen, Überlegungen oder Hürdenmachen– all dies könnt ihr in unserem Blog oder unter "Das Projekt" verfolgen. Bei Fragen könnt Ihr gern mit uns Kontakt aufnehmen.
