+++
fragment = "content"
title = "App-Entwicklung"
title_align = "left"
date = "2021-02-24"
weight = 100
+++

Sie können die KiezRadar-App selbst auf Ihr Handy installieren und ausprobieren.

Die App kann schon so einiges: Ereignisse anzeigen, auch auf einer Karte und die Menge der Ereignisse über Filter einschränken.

Die Installation funktioniert derzeit nur direkt über das zugehörige APK, das heißt, Sie laden die APK-Datei herunter und installieren sie direkt auf Ihrem Handy.

Eine Anleitung dazu finden Sie auf der gitlab-Seite der App:

- [Anleitung zur Installation der KiezRadar-App](https://gitlab.com/kiezradar/app/-/blob/main/apk.md)
- [direkter Download des APK 0.2](https://gitlab.com/kiezradar/app/uploads/bde163d82247b9d01b05ee5774984f45/kiezradar-0.2.apk)

Wir wünschen viel Spaß mit der App, für Feedback, Wünsche und Probleme steht in gitlab ein eigener Bereich zur Verfügung:

- [gitlab Ticketbereich](https://gitlab.com/kiezradar/app/-/issues)

Wir sind derzeit noch in der rechtlichen Klärung des Offenlegung des Codes für die App und für das Backend, sobald das geklärt ist, wird der gitlab-Bereich auch mit Code gefüllt werden.
