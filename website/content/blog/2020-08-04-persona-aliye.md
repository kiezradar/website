+++
fragment = "content"
weight = 100
date = "2020-08-04"

title = "Persona Aliye - Engagiert, gut vernetzt und wenig technikaffin"
title_align = "left"
+++

Die Persona-Methodik hilft uns dabei, eine App zu entwickeln, bei der Bedürfnisse von Benutzer:innen im Fokus stehen.
Details dazu, sowie die Personas Nora und Florian sind in vergangenen Blogeinträgen zu finden.
Eine weitere Persona, also potentielle Nutzerin der KiezRadar-App, stellen wir heute vor.

<!--more-->

Aliye ist im Gegensatz zu Florian und Nora nicht sehr technikversiert und stellt deshalb andere Anforderungen an die KiezRadar-App.
Die Persona-Methodik ermöglicht es, uns in Nutzer:innengruppen hineinzuversetzen, wodurch wir Anforderungen identifizieren und im App-Design berücksichtigen können.


<div class="table-responsive">
	<table class="table bg-white table-bordered">
	<colgroup>
		<col style="width: 50%">
		<col>
	</colgroup>
		<tbody>
			<tr>
				<td>
					<a href="/images/blog/2020-08-04-Persona-Aliye.png"><img src="/images/blog/2020-08-04-Persona-Aliye.png" class="figure-img img-fluid rounded" alt="Persona Aliye" width="30%" ></a>
					<p>Motto:</p>
					<p>"Meine direkte Umgebung möchte ich mitgestalten."</p>
				</td>
				<td>
					<p><em>Aliye</em></p>
					<p><em>49 Jahre</em></p>
					<p><em>Kommt aus Erzurum (Türkei), lebt im Wedding</em></p>
					<p><em>Geschieden, 3 Kinder</em></p>
					<p><em>Laborhelferin</em></p>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Charakteristika</th>
				<th>Anforderungen und Bedürfnisse</th>
			</tr>
			<tr>
				<td>
					<p>
						Aliye ist Laborhelferin und ein lebhafter, herzlicher Mensch mit einer pragmatischen Herangehensweise.
						Sie ist gut darin, zu vernetzen und setzt auf persönliche Kontakte und ihre soziale Ader, um Lösungen zu finden.
						Obwohl ihr Deutsch nicht perfekt ist findet sie sich gut im Alltag zurecht.
						Formulare lässt sie jedoch von ihren Kindern ausfüllen, da sie einige Wörter nicht versteht.
						Aliye wohnt seit Jahrzehnten in einem Weddinger Viertel und trägt dort zu einem lebhaften Kiez bei.
						Ihre Familie ist ihr sehr wichtig, genauso wie beruflicher Erfolg und gute Bildung für ihrer Kinder.
						Vernetzung und Engagement in der Nachbarschaft machen einen großen Teil ihrer Lebensqualität aus.
						Aliye kann gut mit Smartphone und Tablet umgehen, ihre Fähigkeiten am PC sind dagegen begrenzter.
						Ihre Freund:innen schätzen sie als geduldige Zuhörerin, für ihre guten Ratschläge und ihre Verlässlichkeit und sehen sie als Vorbild.
					</p>
				</td>
				<td>
					<ul>
						<li>Informationskanäle: Radio, Türkische Online-Zeitung, Austausch mit Bekannten</li>
						<li>Informationsbedürfnis: gering, möchte über Dinge informiert werden, die sie und ihr Umfeld betreffen</li>
						<li>Sicherheitsbedürfnis: gering in Bezug auf Datensicherheit, wünscht sich Sicherheit in Bezug auf das Kiezleben, das durch Gentrifizierung bedroht wird</li>
						<li>App-Nutzung: besitzt Smartphone, um über Instant-Messenger zu kommunizieren. Benutzt nur Apps mit einfacher und intuitiver Benutzeroberfläche, da sie sich sonst schnell überfordert fühlt.</li>
					</ul>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Ziele und Aufgaben</th>
				<th>Motivation</th>
			</tr>
			<tr>
				<td>
					<ul>
						<li>Interessen/Hobbies: Backen, Kochen, mit Freund:innen treffen, töpfern, reisen, shoppen</li>
						<li>Lebensziele:  ein gutes Leben für sich, Familie und Freunde</li>
						<li>Politisches Engagement: ehemals Vorstand CHP, setzt sich in einer Nachbarschaftsinitiative  gegen Verdrängung aus ihrem Kiez ein und nimmt an Aktivitäten der türkischen Gemeinde in ihrer Nachbarschaft teil</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>Vorbilder: Mustafa Kemal Atatürk, Orhan Gencebay, Halide Edip Adıvar, Meryl Streep</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</div>
