+++
fragment = "content"
weight = 100
date = "2020-10-08"

title = "Wie kann ich die KiezRadar-App nutzen?"
title_align = "left"
+++

Um diese Frage anhand von Beispielen zu veranschaulichen haben wir drei Anwendungsszenarien erstellt.

<!--more-->

Bei den Szenarien war uns wichtig, die drei Datenquellen, auf die wir uns bei dem Prototypen der KiezRadar-App fokussieren, jeweils mit einem Anwendungsfall abzubilden: [meinBerlin](/project/#beispiel-meinberlin), die [Ratsinformationssysteme](/project/#beispiel-ratsinformationssystem) der Bezirke und den Veranstaltungskalender des [Quartiersmanagement Auguste-Viktoria-Allee](/project/#beispiel-quartiersmanagement).

Anwendungsszenarien dienen dazu, die angestrebte Interaktion zwischen den Nutzer:innen und dem System ins Zentrum zu rücken.
Dadurch lassen sich die Interaktionsschritte bspw. kritisch betrachten oder veranschaulichen.

Anwendungsszenerien können auf verschiedene Weise erstellt werden.
Wir haben uns dafür entschieden, die Szenarien als strukturierten Text zu verfassen.
In diesem Fall wird der Ablauf in Schritte aufgeteilt und eine klare Reihenfolge festgelegt.
Neben diesem "Normalablauf" können eventuelle alternative Wege separat dargestellt werden.
Gegenüber der Freitextvariante erleichtert der strukturierte Text bspw. den Vergleich verschiedener Anwendungsfällen und fördert eine präzisiere Ausarbeitung der Szenarien.

Die Anwendungszenarien findet ihr auf der [Projektseite](/project)
