+++
fragment = "content"
weight = 100
date = "2020-10-30"

title = "KiezRadar-Testphase goes Digital?!"
title_align = "left"
+++

Durch die aktuelle Situation rund um Corona versuchen wir auch andere Wege zu gehen und neue digitale Formate ausprobieren.

<!--more-->

Nach unserem digitalen Verwaltungsworkshop möchten wir nun auch die Evaluierung des Prototypen der KiezRadar-App digital umsetzen.
Damit ihr die App bequem von Zuhause aus oder aus dem Office testen könnt, prüfen wir derzeit verschiedene Tools, die genau das ermöglichen.

In diesem Zusammenhang geht es vor allem um Werkzeuge für uns als Anwendungsentwickler, mit denen wir die KiezRadar-App gemeinsam nutzbar machen können.
So können wir interaktive Demos der App anbieten, die dann von Handys, Laptops oder auch Tablets aus in jedem Webbrowser ausgeführt und bedient werden können.

Wenn Ihr Interesse habt die App auf diese Weise zu testen, schreibt uns gerne eine Mail.
Ab Mitte November vergeben wir erste Testslots.
Termine können individuell mit den Interessent:innen abgestimmt werden.
In den ca. 45 minütigen Slots sollen die Testteilnehmer:innen den Prototypen explorieren und Feedback zu bestimmten Aspekten der App geben können.
