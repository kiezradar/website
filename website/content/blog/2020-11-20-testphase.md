+++
fragment = "content"
weight = 100
date = "2020-11-20"

title = "Voller Erfolg der ersten KiezRadar Testphase!"
title_align = "left"
+++

Im Rahmen der Berlin Science Week 2020 fand die erste Testphase der KiezRadar App statt.
Wir haben uns über das große Interesse an einem ersten Ausprobieren der App gefreut und konnten dadurch zahlreiche Evaluationen mit Testpersonen durchführen.

<!--more-->

Unsere App zum ersten Mal "in Action" und in der Interaktion mit Nutzer:innen zu sehen, war auch für uns spannend.
Unser erstes Fazit: wir sind auf dem besten Weg zu einer nutzerzentrierten Anwendung, die bürgerliche Beteilung einfacher und zugänglicher macht!

Die Testphase ermöglicht uns, die Benutzerfreundlichkeit des Prototypen zu evaluieren und dementsprechend Veränderungen vorzunehmen.
Die Testphase hat uns beispielsweise gezeigt, dass...

... Nutzer:innen mit der Menüführung der Anwendung insgesamt gut zurecht kommen.

... eine Aufteilung zwischen "Eigene" und "Alle" in der Suche und beim Entdecken nicht für alle Nutzer:innen verständlich ist.

... Nutzer:innen sich neben der "Entdecken"-Funktion eine separate "Suchen"-Funktion wünschen.

... eine Benachrichtigungsfunktion von Nutzer:innen genutzt werden würde.

... manche Nutzer:innen eine gesonderte Startseite bevorzugen, während für andere Nutzer:innen die "Entdecken"-Seite eine passende Startseite ist.

... viele Nutzer:innen sehen möchten, wie beliebt Projekte und Events bei anderen Nutzer:innen sind.
