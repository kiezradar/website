+++
fragment = "content"
weight = 100
date = "2020-12-02"

title = "KiezRadar Testphase: Erkenntnisse aus der Auswertung"
title_align = "left"
+++

Für die KiezRadar Testphase haben wir eine Liste von Hypothesen in Bezug auf Menüführung, Sprache und Design der KiezRadar App erstellt.
Während der Tests mit Nutzer:innen wurden die Hypothesen durch gezielte Fragestellungen adressiert, um evidenzbasiert Hypothesen zu bestätigen oder abzulehnen.

<!--more-->

Ein Auszug aus der Hypothesenauswertung zeigt Folgendes:

<div class="table-responsive">
	<table class="table bg-white table-bordered">
		<thead>
			<tr class="thead-light">
				<th>Hypothese</th>
				<th>Ergebnis</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<p>Die Nutzer:innen wünschen eine Aufteilung in mehrere Kategorien in der "Entdecken" Ansicht (aktuell, beliebt, eigene).</p>
				</td>
				<td>
					<p>Viele Nutzer:innen verstehen nicht intuitiv, was der Reiter "Eigene" bezeichnet. Manche Nutzer:innen finden die "Entdecken" Ansicht durch die mehreren Reiter überladen.  Die Bedeutung von "Aktuell" und "Beliebt" wird richtig verstanden und als sinnvoll empfunden.</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Die Nutzer:innen möchten in der "Entdecken" Ansicht" gerne Bilder zu den Beteiligungen und die wichtigesten Infos gezeigt bekommen.</p>
				</td>
				<td>
					<p>Ein Galeriebild in der Listenansicht kommt bei Nutzer:innen gut an, genauso wie eine kurze Informationsübersicht.</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Die Nutzer:innen möchten gerne schnell erfassen und angezeigt bekommen, welche Beteiligungen bei anderen Bürger:innen besonders beliebt sind.</p>
				</td>
				<td>
					<p>Einige Nutzer:innen wünschen sich eine Information zur Beliebtheit von Einträgen. Manche Nutzer:innen fühlen sich durch eine Beliebtheitsanzeige beeinflusst und bewerten diese deshalb negativ.</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nutzer:innen wünschen sich eine separate Startseite.</p>
				</td>
				<td>
					<p>Eine Mehrheit der Nutzer:innen wünschen keine separate Startseite, sondern möchten mit der "Entdecken" Ansicht starten. </p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nutzer:innen wünschen sich eine separate Suchfunktion als eigenen Menüreiter.</p>
				</td>
				<td>
					<p>Eine separate Suchfunktion wird dringend gewünscht. </p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nutzer:innen wünschen sich Benachrichtigungen durch die App. </p>
				</td>
				<td>
					<p>Die meisten Nutzer:innen finden Benachrichtigungen sinnvoll und würden diese nutzen.</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>Nutzer:innen wünschen sich eine separate Information über alle Projektfortschritte, nicht nur über ihre Favoriten.</p>
				</td>
				<td>
					<p>Die meisten Nutzer:innen halten eine unterteilte Anzeige der Projektfortschritte für überladen und wünschen sich eine vereinfachte Lösung. </p>
				</td>
			</tr>
		</tbody>
	</table>
</div>
