+++
fragment = "content"
weight = 100
date = "2021-04-01"

title = "short bite digital social summit"
title_align = "left"
+++

Beim diesjährigen Digital Social Summit war das Kiezradar-Projekt mit einem "Short Bite" vertreten.
In dem kurzen Video stellt Projektleiterin Susanna Kuper das Projekt, die Problemstellung und Ziele vor.

<!--more-->

Zudem erzählt sie über das Corona-Jahr 2020 und wie das Thema Nachhaltigkeit im Projekt KiezRadar aufgegriffen wird.
Das Video findet ihr [auf Youtube](https://youtu.be/PutuLj6SSw8) oder gleich hier:

<iframe width="560" height="315" src="https://www.youtube.com/embed/PutuLj6SSw8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
