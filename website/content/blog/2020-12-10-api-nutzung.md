+++
fragment = "content"
weight = 100
date = "2020-12-10"

title = "Die Kiezradar API - wie sie benutzt wird und was sie kann"
title_align = "left"
+++

Im Beitrag über [API-Design](/blog/2020-06-10-api-design/) wurde der Aufbau der KiezRadar-API vorgestellt.
Die API ist mittlerweile fertig und kann ausprobiert und benutzt werden!

<!--more-->

Im heutigen Beitrag werden einige Beispielabfragen durch den fiktiven Charakter Sophie vorgestellt.
Sophie wohnt in Berlin, interessiert sich für Bürgerbeteiligung und möchte die API direkt nutzen, um zu entscheiden, ob sie eine eigene App oder Browser-Anwendung aufgrund der Daten schreiben möchte.
Sie möchte wissen, welche Daten in der KiezRadar API zu Verfügung stehen und stellt deshalb ein paar Abfragen.

Die API liefert JSON-Daten zurück, die durch Computer leicht verarbeitet werden können.
Sie sind zwar für Menschen lesbar, oft aber nicht sofort verstehbar.
Die Daten werden in diesem Text als Screenshots hinterlegt, die Links könnt Ihr jedoch selbst öffnen und die Daten ansehen.
Zurück zu Sophie...

Zuerst möchte Sophie sich einen Überblick über die API verschaffen.
Sie ruft deshalb <https://kiezradar.fokus.fraunhofer.de/api/> auf und bekommt die automatisch generierte Schnittstellenbeschreibung.
Eine Dokumentation ist in Arbeit:

<figure class="figure">
  <a href="/images/blog/2020-12-10-01.png"><img src="/images/blog/2020-12-10-01.png" class="figure-img img-fluid rounded" alt="Schnittstellenbeschreibung." width="50%"></a>
  <figcaption class="figure-caption">Schnittstellenbeschreibung.</figcaption>
</figure>

Als nächstes möchte Sophie die Veranstaltungen und Projekte anschauen.
Sie vermutet (korrekt), dass diese über "event" oder "eventreducedinformation" zu finden sind.
Über <https://kiezradar.fokus.fraunhofer.de/api/event> erreicht sie alle Einträge in diesem Bereich:

<figure class="figure">
  <a href="/images/blog/2020-12-10-02.png"><img src="/images/blog/2020-12-10-02.png" class="figure-img img-fluid rounded" alt="Event-Liste." width="50%"></a>
  <figcaption class="figure-caption">Event-Liste.</figcaption>
</figure>

Ein Blick in die mitgelieferten Header verrät ihr u.a., dass 1083 Einträge gefunden wurden:

<figure class="figure">
	<a href="/images/blog/2020-12-10-03.png"><img src="/images/blog/2020-12-10-03.png" class="figure-img img-fluid rounded" alt="Event-Liste Header." width="50%"></a>
	<figcaption class="figure-caption">Event-Liste Header.</figcaption>
</figure>

In den Einträgen findet Sie das Event "Interkulturelle Woche: Malen im LAIV".
Dieses Projekt war zwar schon im Oktober, interessiert sie aber näher, daher ruft sie Einzelheiten des Events über dessen UUID auf.
Mit <https://kiezradar.fokus.fraunhofer.de/api/event?uuid=eq.2128dc16-0f51-9b9a-52b5-c08eefc9a1ab> bekommt Sophie Informationen über das Projekt, u.a. Titel und Zeitraum:

<figure class="figure">
	<a href="/images/blog/2020-12-10-04.png"><img src="/images/blog/2020-12-10-04.png" class="figure-img img-fluid rounded" alt="Event-Details." width="50%"></a>
	<figcaption class="figure-caption">Event-Details.</figcaption>
</figure>

Der Ort des Projekts ist nur als UUID hinterlegt, außerdem sind viele Informationen derzeit für Sophie uninteressant.
Daher filtert sie die Ergebnisse nach Titel, Koordinaten und Ereignistyp mit <https://kiezradar.fokus.fraunhofer.de/api/event?uuid=eq.2128dc16-0f51-9b9a-52b5-c08eefc9a1ab&select=title,location(coordinate),eventtype(title)>:

<figure class="figure">
	<a href="/images/blog/2020-12-10-05.png"><img src="/images/blog/2020-12-10-05.png" class="figure-img img-fluid rounded" alt="Event: Titel, Ort, Typ." width="50%"></a>
	<figcaption class="figure-caption">Event: Titel, Ort, Typ.</figcaption>
</figure>

Sophie geht jetzt zur Liste aller Events zurück und möchte diese nach einer bestimmten Art von Veranstaltungen und Projekten filtern.
Sie möchte Einträge abrufen, die den Themen (sections) "Quartiersmanagement" oder "Bildung und Forschung" zugeordnet sind.
Hier reicht ihr erst einmal die Anzeige der Eventtitel.
Dafür ruft sie <https://kiezradar.fokus.fraunhofer.de/api/section?title=in.(%22Bildung%20%26%20Forschung%22,%22Quartiersmanagement%22)&select=title,event!nmeventsection(title)> auf und erhält:

<figure class="figure">
	<a href="/images/blog/2020-12-10-06.png"><img src="/images/blog/2020-12-10-06.png" class="figure-img img-fluid rounded" alt="Gefilterte Event-Liste, nur Titel." width="50%"></a>
	<figcaption class="figure-caption">Gefilterte Event-Liste, nur Titel.</figcaption>
</figure>

Damit ist Sophie zufrieden und kann entscheiden, ob sie die KiezRadar-App oder lieber die API direkt nutzen möchte.

Dieser Artikel hat einen kleinen Einblick über die Möglichkeiten der Datenabfrage mit Hilfe der KiezRadar-API gegeben.
Es ist noch viel mehr möglich - probiert es einfach aus.
