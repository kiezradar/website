+++
fragment = "content"
weight = 100
date = "2020-06-10"

title = "API-Design"
title_align = "left"
+++

Die Schnittstelle (API), über die die KiezRadar-App auf die Daten zugreift, liegt in unserer Hand und muss entwickelt werden.

Dabei sind mehrere Dinge zu beachten: ein geeignetes Format, die Auswahl der zu übermittelnden Daten, die Bereitstellung der Daten und der Test der Schnittstelle.

<!--more-->

Als Format haben wir uns sehr schnell für eine REST-Schnittstelle im JSON-Format entschieden. Das ist derzeit Stand der Technik, bewährt und im Rahmen von KiezRadar auch gut geeignet. Da wir eine Schnittstelle für Behördendaten entwickeln, betrachten wir zusätzlich den OParl-Standard und prüfen, ob unsere Schnittstelle dem Standard entsprechen soll. Derzeit sieht das gut aus, es sind aber noch Detailfragen zu klären.

Die Auswahl der zu übermittelnden Daten ergibt sich derzeit aus den Mockups, in denen wir die Verwendung der Daten skizzieren. Die dort angezeigten Daten müssen entsprechend auch von der Schnittstelle angeboten werden. Außerdem geben die unterschiedlichen Mockups auch Hinweise darauf, welche Ressourcen die Schnittstelle zur Verfügung stellen muss.

Die Daten selbst werden in der jetzigen Planung aus einer eigenen Datenbank zur Verfügung gestellt, die regelmäßig aus den öffentlichen Datenquellen befüllt wird. So ist das Projekt unabhängig von den Datenquellen und deren Formatänderungen. Andererseits entspricht die Aktualität der Daten dann auch nur den Aktualisierungsintervallen. Bei den in KiezRadar angepeilten Daten ist ein Aktualisierungintervall von einem Tag in Ordnung. Das Befüllen der Datenbank wird prorotypisch über direkte Abfrage und Datenumwandlung vorgenommen, bis Datenbank und Schnittstelle stabil sind. Dann wird dieser Teil des Systems neu betrachtet.

Der Test der Schnittstelle ist über drei Wege geplant: zunächst über den App-Prototyp, der auf Android-Basis implementiert wird. Parallel dazu wird ein HTML-Prototyp gebaut, der ausschließlich dem Test und der Demonstration der Nutzung der Schnittstelle gewidmet ist. Als drittes werden Regressionstests mit Hilfe des Karate-Testframeworks durchgeführt werden.

Welche technischen Schwierigkeiten dabei auftreten und wie diese gelöst werden bzw. wurden, erzählen wir in einem späteren Blogbeitrag.
