+++
fragment = "content"
weight = 100
date = "2020-06-03"

title = "Hallo Persona Nora - wie Personas bei der Entwicklung eines nutzerzentrierten Designs der Kiezradar-App helfen"
title_align = "left"
+++

Personas werden in vielen Bereichen eingesetzt.
Auch in der agilen Softwareentwicklung oder im Design Thinking Ansatz haben sie mittlerweile ihren festen Platz.
Sie helfen, Probleme aus der Sicht der Nutzer:innen zu sehen und dies bei der Entwicklung eines Produkts oder Services zu berücksichtigen.

<!--more-->

Hierfür werden zuerst Daten erhoben, meist auf Basis von Recherchen und aus Interviews mit potentiellen Nutzer:innen.
Im zweiten Schritt werden daraus Personas erstellt.
Personas sind fiktive Charaktere, die die erhobenen Informationen clustern und typische Nutzergruppen repräsentieren.

Eine Persona besteht aus

- demographischen Infos, z. B. Alter und Beruf
- persönlichen Details, z. B. Kurzbiografie, Name
- Einstellungen und Werten, z. B. Wichtigkeit von Gemeinschaft
- Zielen und Motivationen in Bezug auf das Produkt, z. B. die Suche nach Bestimmten Informationen, um sich einfacher zu beteiligen
- Verhaltensweisen in Situationen mit dem Produkt, z. B. wenig Geduld im Umgang mit Apps

Personas sind wichtig, um ein nutzerzentriertes Design in der Produktentwicklung zu realisieren.
Sie helfen den Entwickler:innen dabei, sich in die Nutzer:innen hineinzuversetzen, da sie die Nutzergruppen mithilfe fiktiver Charaktere "zum Leben erwecken" und so potentielle Bedürfnisse, Probleme und "pain points" in Bezug auf das Produkt sichtbar machen.

In der Entwicklung der Kiezradar App wird deshalb der Persona-Ansatz genutzt.
Die Personas begründen sich auf Daten aus Interviews mit verschiedenene Repräsentanten der jeweiligen Nutzergruppen, die im Fokus der Appentwicklung stehen.
Zusätzlichen sind Informationen, die wir während des Bürgerworkshops sammeln konnten, mit in die Erstellung der Personas eingeflossen.
Einen Überblick über alle Personas findet Ihr auf unserer Projektseite.
Von den Personas, die in unserem Entwicklungsprozesses erstellt wurden, ist eine die Persona Nora.
Wir stellen vor - Persona Nora:

<div class="table-responsive">
	<table class="table bg-white table-bordered">
	<colgroup>
		<col style="width: 50%">
		<col>
	</colgroup>
		<tbody>
			<tr>
				<td>
					<a href="/images/blog/2020-06-03-Persona-Nora.png"><img src="/images/blog/2020-06-03-Persona-Nora.png" class="figure-img img-fluid rounded" alt="Persona Nora" width="30%" ></a>
					<p>Motto:</p>
					<p>"Eine gute Anwendung ist Open Source und achtet auf Datenschutz."</p>
					<p>"Ich möchte meine Fähigkeiten einsetzen, um eine gerechtere Gesellschaft mitzugestalten."</p>
				</td>
				<td>
					<p><em>Nora</em></p>
					<p><em>33 Jahre</em></p>
					<p><em>Kommt aus Rostock, lebt im Prenzlauer Berg</em></p>
					<p><em>ledig</em></p>
					<p><em>Netzwerkadministration und IT—Support</em></p>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Charakteristika</th>
				<th>Anforderungen und Bedürfnisse</th>
			</tr>
			<tr>
				<td>
					<p>
						Nora lebt nun schon seit 10 Jahren in Berlin und arbeitet hier als Netzwerkadministratorin und IT-Supporterin.
						Sie ist ein energischer und umtriebiger Mensch, vernetzt sich gut und hat eine offene und neugierige Lebenseinstellung.
						Es ist ihr wichtig, eigenständig und unabhängig von großen Konzernen zu leben, wofür sie technische Hürden gerne auf sich nimmt, wie beispielsweise die Benutzung eines eigenen Servers.
						Gegenseitige Unterstützung und Teilen von Fähigkeiten, Wissen und Infrastruktur haben eine hohe Priorität für sie.
						Sie nimmt deshalb regelmäßig an regionalen und überregionalen Veranstaltungen und Aktionen der Netzpolitik-Community teil und setzt sich für Open Data und Open Source ein.
						Immaterielle Werte, wie das Anstreben eines guten Lebens, stehen für Nora über materiellen.
						Durch Ausbildung und Beruf besitzt sie fortgeschrittene technische Fähigkeiten, beherrscht mehrere Programmiersprachen (u. a. C++, Java) und erkundet neugierig technische Hintergründe, um ihr Verständnis zu erweitern.
						In ihrem Freundeskreis gibt sie Impulse und ist eine kritische Stimme, die über technische Zusammenhänge, wie Sicherheitslücken, aufklärt und gerne auch mal die Rolle der Kommunikatorin übernimmt.
					</p>
				</td>
				<td>
					<ul>
						<li>Informationskanäle: rein digital, Videos, Podcasts, Blogs, Newsfeeds, E-Books (Sachbücher)</li>
						<li>Informationsbedürfnis: Hoch, infomiert sich über aktuelle Nachrichten, Info-Junkie: hohes Informationsbedürfnis wird durch die Kombination viele digitaler Medien gestillt.</li>
						<li>Sicherheitsbedürfnis: In Bezug auf Datensicherheit hoch, sonst eher gering, offen für Neues</li>
						<li>App-Nutzung: Funktion ist wichtiger als Design, sehr überlegte Installation von Apps, aber nutzt relativ viele Apps, temporäre Installation von Apps, probiert auch mal was aus, bezahlt auch gerne für eine App. Funktion vor Design: bei der App-Nutzung ist die Funktionalität entscheidend.</li>
					</ul>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Ziele und Aufgaben</th>
				<th>Motivation</th>
			</tr>
			<tr>
				<td>
					<ul>
						<li>Interessen/Hobbies: DIY-Projekte (Löten, Arduino, Microkontroller, 3D-Druck u.Ä.) elektronische Musik, Organisation und -teilnahme an Open Air Musik Festivals</li>
						<li>Lebensziele: gute Beziehungen pflegen, an Kultur und Stadtleben teilhaben, gesellschaftlich oder politisch wirken für ein gerechteres Zusammenleben</li>
						<li>Politisches Engagement: Demos, Clubvereinigung (Bündnis gegen das Sterben der Berliner Clubkultur), aktiv im Verein für feministische Netzpolitik</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>Vorbilder: Edward Snowden, Margaret Hamilton, Pipi Langstrumpf, Linus Neumann, Mai Thi Nguyen-Kim</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</div>
