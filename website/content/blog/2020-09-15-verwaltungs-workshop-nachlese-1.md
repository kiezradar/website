+++
fragment = "content"
weight = 100
date = "2020-09-15"

title = "Workshop mit Verwaltungsmitarbeiter:innen am 10. September 2020"
title_align = "left"
+++

Am 10. September 2020 fand der virtuelle KiezRadar Workshop mit Verwaltungsmitarbeiter:innen statt, der den ausgefallenen Workshop im März ersetzt.

<!--more-->

Verwaltungsmitarbeiter:innen aus verschiedenen Bereichen lernten die KiezRadar App kennen und gaben wichtige Impulse aus der Perspektive der Verwaltung.
In dem interaktiven Workshop wurde unter anderem die Umsetzbarkeit von Anforderungen an die App unter Einbeziehung von administrativen Prozessabläufen erarbeitet.
Eine Zusammenfassung der Workshopergebnisse ist in Vorbereitung und wird in Kürze auf der Webseite geteilt.

<figure class="figure">
  <a href="/images/blog/2020-09-15-Team-1.jpg"><img src="/images/blog/2020-09-15-Team-1.jpg" class="figure-img img-fluid rounded" alt="Mockup Entdecken" width="30%"></a>
  <figcaption class="figure-caption">Das KiezRadar-Team vor dem Workshop</figcaption>
</figure>
<figure class="figure">
  <a href="/images/blog/2020-09-15-Conceptboard-Nora.png"><img src="/images/blog/2020-09-15-Conceptboard-Nora.png" class="figure-img img-fluid rounded" alt="Mockup Entdecken" width="30%"></a>
  <figcaption class="figure-caption">Ergebnis der Diskussion: Persona Nora</figcaption>
</figure>
<figure class="figure">
  <a href="/images/blog/2020-09-15-Team-2.jpg"><img src="/images/blog/2020-09-15-Team-2.jpg" class="figure-img img-fluid rounded" alt="Mockup Entdecken" width="30%"></a>
  <figcaption class="figure-caption">Das KiezRadar-Team nach dem Workshop</figcaption>
</figure>
