+++
fragment = "content"
weight = 100
date = "2020-06-24"

title = "Von der API auf das Display #1"
title_align = "left"
+++

Dieser Beitrag ist aus einer Reihe, in der wir Euch einen Einblick in ausgewählte programmiertechnische Aspekte geben möchten, die uns bei der Umsetzung der Kiezradar App begegnet sind. 

Hierbei werden wir uns bemühen, nach und nach die Frage "Wie kommen die Daten auf mein Handydisplay?" etwas genauer zu beleuchten.

<!--more-->


Für die Beantwortung dieser Frage müssen wir unsere Architektur ansehen. Die Architektur besteht im Prinzip aus drei wichtigen Komponenten

1. Einer Datenbank, die unsere Daten (Informationen zu Beteiligungsmöglichkeiten) enthält.
2. Einer API (Schnittstelle), über die von Apps auf die Datenbank zugegriffen werden kann, sie wurde bereits etwas genauer in einem vorherigen Blogbeitrag ([API-Design](https://kiezradar.fokus.fraunhofer.de/blog/2020-06-10-api-design/)) beschrieben.
3. Der App, die die Daten über die API herunterlädt, verarbeitet und anzeigt.

Aus Sicht der App ist somit der erste Schritt der Zugriff auf die Daten mittels API-Aufruf (API-Call). Dieser Call wird mit Hilfe eines HTTP-Request implementiert. 

Das Beispiel eines GET-Request über die Volley-HTTP-Bibliothek für Android sieht wie folgt aus:

```kotlin
StringRequest(Request.Method.GET, url,
	Response.Listener<String> { response ->
		// Display the first 500 characters of the response string.
		textView.text = "Response is: ${response.substring(0, 500)}"
	},
	Response.ErrorListener { textView.text = "That didn't work!" }
)
```

(Quelle: <https://developer.android.com/training/volley/simple>)

In einer Android App kann der Aufruf allerdings auch mit der Erweiterung *java.net.URL* implementiert werden. Auch diese Bibliothek bietet die Möglichkeit, zusammen mit *java.net.HttpURLConnection* HTTP-Anfragen mit Parameterübergabe und Responsecodeüberprüfung zu implementieren.

Im einfachsten Fall kann man so die API mit einem GET-Aufruf aufrufen:

```kotlin
fun URL.readText(charset: Charset = Charsets.UTF_8): String 
```

(Quelle: <https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.io/java.net.-u-r-l/read-text.html>)

Für erste Tests ist dieser Aufruf ausreichend, jedoch bei größeren Dokumenten nicht zu empfehlen. Auch fehlt dabei die Implementierung der Fehlerbehandlung, beispielsweise wenn die aufgerufene Ressource über die Schnittstelle nicht erreichbar ist und ein Fehlercode zurückgegeben wird.

Wir benutzen die Volley-HTTP-Bibliothek, weil die API-Anfragen sehr einfach und vor allem sehr schnell zu implementieren sind. Zudem bietet Volley die Möglichkeit der Individualisierung, Priorisierung und Löschung einzelner Anfragen.

Derzeit setzen wir die Designvorschläge der [Mockups](https://kiezradar.fokus.fraunhofer.de/project/#mockups) um. Einige Grundfunktionen, die Datenabfragen und Datenverarbeitung betreffen, sind bereits implementiert. Nach dem das Design umgesetzt und das Backend weiter ausgebaut ist, werden weitere Funktionen umgesetzt.
