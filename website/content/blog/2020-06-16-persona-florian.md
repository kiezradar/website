+++
fragment = "content"
weight = 100
date = "2020-06-16"

title = "Persona Florian - jung und interessiert"
title_align = "left"
+++

Im [Beitrag vom 3. Juni 2020](/blog/2020-06-02-persona-nora/) haben wir die Persona-Methodik vorgestellt, die wir für die KiezRadar-App nutzen, zusammen mit der [Persona Nora](/blog/2020-06-02-persona-nora/).
Jede Persona bildet eine potentielle Nutzer:innengruppe ab.
Heute präsentieren wir die Persona Florian:

<!--more-->

<div class="table-responsive">
	<table class="table bg-white table-bordered">
	<colgroup>
		<col style="width: 50%">
		<col>
	</colgroup>
		<tbody>
			<tr>
				<td>
					<a href="/images/blog/2020-06-16-Persona-Florian.png"><img src="/images/blog/2020-06-16-Persona-Florian.png" class="figure-img img-fluid rounded" alt="Persona Florian" width="30%" ></a>
					<p>Motto:</p>
					<p>"Begegnung und Austausch sind mir wichtig."</p>
					<p>"Ich wünsche mir transparente Entscheidungsfindungen und eine lebendige Gemeinschaft."</p>
				</td>
				<td>
					<p><em>Florian</em></p>
					<p><em>25 Jahre</em></p>
					<p><em>Kommt aus Freiburg, lebt in Neukölln</em></p>
					<p><em>ledig</em></p>
					<p><em>Student (Kulturwissenschaften)</em></p>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Charakteristika</th>
				<th>Anforderungen und Bedürfnisse</th>
			</tr>
			<tr>
				<td>
					<p>
						Florian studiert Kulturwissenschaft und ist für sein Studium nach Berlin gezogen.
						Er nutzt seine Freizeit gerne, um sich gesellschaftlich einzubringen und engagiert sich politisch in verschiedenen Initiativen.
						Dort wird er für seine reflektierte und unterstützende Persönlichkeit geschätzt.
						Er hat viele Interessen, ein großes Verantwortungsgefühl und eine Leidenschaft dafür, Dinge anzupacken, ist jedoch ein wenig chaotisch.
						Ein gutes Zusammenleben hat einen hohen Stellenwert für Florian, weshalb ihm transparente Entscheidungsfindungen, rationale Debatten, Orientierung am Gemeinwohl und Toleranz sehr wichtig sind.
						Er lebt gerne in Berlin, da die Stadt viele Möglichkeiten und Facetten bietet, findet aber, dass viele Dinge verbessert werden können.
						Er wünscht sich zudem, dass sich mehr Menschen an der Gestaltung des urbanen Zusammenlebens beteiligen.
						Auf seinem Smartphone nutzt er verschiedene Apps, jedoch immer mit einem kritischen Blick auf Datenschutz, wofür er sich auch mal in technische Details einarbeitet.
					</p>
				</td>
				<td>
					<ul>
						<li>Informationskanäle: digital und analog, Videos und Audio (Podcasts), Bücher (auch Sachbücher), Zeitung</li>
						<li>Informationsbedürfnis: hoch, aber nimmt nicht immer teil, vor allem im Kontext "Wem gehört die Stadt"/Stadt/Architektur.</li>
						<li>Sicherheitsbedürfnis: nicht ängstlich, lebt in einem sicheren Land, offen für Neuerungen, Freiheiten wichtiger als Sicherheiten</li>
						<li>App-Nutzung: eher ausgewählt Nutzung, die regelmäßig genutzt werden, und die gut funktionieren sollen, hat keine Lust sich groß einzuarbeiten</li>
					</ul>
				</td>
			</tr>
			<tr class="thead-light">
				<th>Ziele und Aufgaben</th>
				<th>Motivation</th>
			</tr>
			<tr>
				<td>
					<ul>
						<li>Interessen/Hobbies: Klettern, Lebensmittel retten, Holzbau, Imkern</li>
						<li>Lebensziele: Was gutes Hinterlassen für die Gemeinschaft, gemeinschaftliches Leben, Familie</li>
						<li>Politisches Engagement: ehemals bei den Grünen, Mitarbeit bei einem Berlin Volksentscheid zum Thema Radinfrastruktur, geht regelmäßig auf Demos, Vorstand eines Vereins zur Berliner Stadtraumkultur</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>Vorbilder: Hannah Arendt, Noam Chumsky, Margarete Stokowski</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</div>
