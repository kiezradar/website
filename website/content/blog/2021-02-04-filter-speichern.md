+++
fragment = "content"
weight = 100
date = "2021-02-04"

title = "Filter speichern"
title_align = "left"
+++

Bei den Nutzertests kam das erste Mal der Wunsch nach einer Möglichkeit zur Speicherung der gesetzten Filtereinstellungen auf.

<!--more-->

Der Mehrwert einer solchen Funktion besteht darin, dass einmal gesetzte Filter nicht erneut eingegeben werden müssen und das schneller zwischen verschiedenen Filtereinstellungen hin und her gewechselt werden kann.
Positive Effekte der Speicher-Funktion können sein, dass die Filterfunktion mehr genutzt wird, um ein schnelles Auffinden passender Ereignisse zu erzielen.
Zudem kommt es denen entgegen, die mit der Bedienung von Apps weniger vertraut sind: Filter müssen nur einmal gesetzt werden.

Um eine möglichst intuitive Umsetzung der Speicher-Funktion zu erhalten, haben wir uns viele Gedanken gemacht.
Die Funktionsweise soll eindeutig und klar erkennbar gestaltet sein und auch auf eher kleinen Smartphones gut bedient werden können.
Die zentralen Funktionen die untergebracht werden müssen sind die Speicherung und das Öffnen bzw. Auswählen bereits gespeicherter Filter.
Die Funktion, Filter löschen zu können, sollte möglichst auch bei der Filterfunktion untergebracht werden.
Eine Verortung der Löschfunktion unter Einstellung erscheint uns aber auch plausibel.

Eine zentrale Herausforderung bei der Gestaltung der Designideen ergibt sich daraus, dass die Filterfunktion bereits einige Funktionen und Button umfasst.
Zudem entfällt der untere Bereich für die Platzierung der Buttons, da dieser beim Aufklappen der Filterbereiche aus dem Blickfeld verschwinden könnte.

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-5">
			<div class="card-body">
				<h4 class="card-title">Entwurf 1</h4>
				<p class="card-text">
					Bei diesem Entwurf wird die Möglichkeit der Speicherung und bereits gespeicherte Filtereinstellungen öffnen zu können jeweils durch Buttons klar benannt.
					Zudem sind die Funktionen prominent am oberen Rand platziert.
					Die Auswahl der Filtereinstellungen erfolgt über ein separates Feld (Bild 2).
					Die Löschen-Funktion kann über das "Mülleimer"-Symbol oben rechts erfolgen.
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-1-1.png"><img src="/images/blog/2021-02-04-filter-1-1.png" alt="Filterentwurf 1-1." title="Filterentwurf 1-1" class="card-img" /></a>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-1-2.png"><img src="/images/blog/2021-02-04-filter-1-2.png" alt="Filterentwurf 1-2." title="Filterentwurf 1-2" class="card-img" /></a>
		</div>
	</div>
</div>

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-5">
			<div class="card-body">
				<h4 class="card-title">Entwurf 2</h4>
				<p class="card-text">
					Bei dieser Variante kann zwischen den bereits gespeicherten Filtereinstellungen direkt gewechselt werden, indem die Pfeile links oder rechts gedrückt werden.
					Sobald ein neuer Filter gesetzt wird, wird oben die Möglichkeit zur Speicherung durch "Speichern" angezeigt.
					Bild 2 zeigt die Maske zur Festlegung des Filternamens.
					In diesem Entwurf ist die Löschen-Funktion nicht Teil der Filterfunktion.
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-2-1.png"><img src="/images/blog/2021-02-04-filter-2-1.png" alt="Filterentwurf 2-1." title="Filterentwurf 2-1" class="card-img" /></a>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-2-2.png"><img src="/images/blog/2021-02-04-filter-2-2.png" alt="Filterentwurf 2-2." title="Filterentwurf 2-2" class="card-img" /></a>
		</div>
	</div>
</div>

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-5">
			<div class="card-body">
				<h4 class="card-title">Entwurf 3</h4>
				<p class="card-text">
					Dieser Entwurf unterscheidet sich nur durch eine kleine Veränderung vom Entwurf zuvor.
					Statt der expliziten Speicher-Funktion werden die Speicher- und Löschfunktion unter das Drei-Punkte-Symbol subsumiert.
					Für die Auswahl der passenden Funktion öffnet sich ein Extra-Fenster (Bild 2).
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-3-1.png"><img src="/images/blog/2021-02-04-filter-3-1.png" alt="Filterentwurf 3-1." title="Filterentwurf 3-1" class="card-img" /></a>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-02-04-filter-3-2.png"><img src="/images/blog/2021-02-04-filter-3-2.png" alt="Filterentwurf 3-2." title="Filterentwurf 3-2" class="card-img" /></a>
		</div>
	</div>
</div>

Welche Varianten gefallen Euch am besten?
Wo seht ihr Vor- und Nachteile?
Oder hättest Ihr lieber eine andere Form der Umsetzung?
Schreibt uns gerne eine <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#105;&#101;&#122;&#114;&#97;&#100;&#97;&#114;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;"><span class="far fa-envelope" title="E-Mail"></span> E-Mail</a>
