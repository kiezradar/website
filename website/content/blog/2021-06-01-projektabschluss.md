+++
fragment = "content"
weight = 100
date = "2021-06-01"

title = "Projektabschlussbericht"
title_align = "left"
+++

Der Projektabschlussbericht ist fertig und damit ist das Projekt KiezRadar erst einmal beendet.

<!--more-->

Im März haben wir den Prototyp unserer Bürger:innenbeteiligungs-App fertiggestellt.
Alle Erkenntnisse über das Projekt, den Verlauf und die möglichen Fortführungsszenarien haben wir in einem [Projektabschlussbericht](/downloads/Projektabschlussbericht-KiezRadar.pdf) zusammengetragen:

- [Download des Projektabschlussberichts](/downloads/Projektabschlussbericht-KiezRadar.pdf)

Ihr könnt das Projekt noch einmal am 18. Juni 2021 ab 16 Uhr auf dem [Digitaltag 2021](https://digitaltag.eu/) ansehen und diskutieren:

- [KiezRadar auf dem Digitaltag 2021](https://digitaltag.eu/kiezradar-die-app-fuer-mehr-buergerbeteiligung-gemeinsam-mit-nutzerinnen-entwickeln)

Wir bedanken uns für Eure Aufmerksamkeit und die Teilnahme an der App-Entwicklung.

Falls Ihr in die Quellen sehen wollt: sie stehen Euch für alle KiezRadar-Teile auf gitlab zur Verfügung

- <https://gitlab.com/kiezradar/app>
- <https://gitlab.com/kiezradar/backend>
- <https://gitlab.com/kiezradar/website>
