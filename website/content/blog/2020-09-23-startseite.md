+++
fragment = "content"
weight = 100
date = "2020-09-23"

title = "Startseite auf dem Radar"
title_align = "left"
+++

Wir fragen uns schon länger, mit welcher Ansicht die Nutzer:innen die KiezRadar-App starten wollen.

<!--more-->

Möchten sie auf der Startseite eine passende neue Beteiligung vorgestellt bekommen? Möchten sie von einer Zusammenstellung der neusten Ereignisse begrüßt werden? Oder benötigen sie vielleicht gar keine Startseite, sondern starten stattdessen lieber direkt mit der Unterseite "Entdecken", der Suche oder der Kartenfunktion.

Mit dem neuen alternativen Startseiten-Mockup haben wir versucht, die Radar-Metapher von KiezRadar aufzugreifen und den Nutzer:innen einen schnellen Überblick, über für sie relevante Beteiligungen und Veranstaltungen zu geben. Lasst uns gerne ein Feedback dar, welche Startseite Euch besonders gut gefällt.

<figure class="figure">
  <a href="/images/blog/2020-09-23-Startseite.png"><img src="/images/blog/2020-09-23-Startseite.png" class="figure-img img-fluid rounded" alt="Mockup Startseite." width="50%"></a>
  <figcaption class="figure-caption">Mockup Startseite.</figcaption>
</figure>
