+++
fragment = "content"
weight = 100
date = "2020-11-06"

title = "Digitale KiezRadar-Testphase in den Startlöchern"
title_align = "left"
+++

Die KiezRadar-Testphase findet digital statt.
Die letzten Vorbereitungen sind nun getroffen und das KiezRadar-Entwicklungsteam ist gespannt auf erstes Nutzer:innenfeedback für den Prototypen!

<!--more-->

Zum Testen wird die App in einem Emulator ausgeführt, der vom Browser aus bedient werden kann.
Dort können alle Funktionalitäten und die Benutzeroberfläche des aktuellen Prototypen ausprobiert werden.
Das sieht dann beispielsweise so aus:

<figure class="figure">
  <a href="/images/blog/2020-11-06-Startseite.png"><img src="/images/blog/2020-11-06-Startseite.png" class="figure-img img-fluid rounded" alt="Demonstrator Startseite." width="50%"></a>
  <figcaption class="figure-caption">Testapp Startseite.</figcaption>
</figure>
<figure class="figure">
  <a href="/images/blog/2020-11-06-Karte.png"><img src="/images/blog/2020-11-06-Karte.png" class="figure-img img-fluid rounded" alt="Demonstrator Karte." width="50%"></a>
  <figcaption class="figure-caption">Testapp Karte.</figcaption>
</figure>

Alle Informationen zur [Science Week](/science-week) und zur Anmeldung finden Sie auf der [Science-Week-Seite](/science-week).
