+++
fragment = "content"
weight = 100
date = "2020-06-30"

title = "KiezRadar-Workshop goes digital!"
title_align = "left"
+++

Nachdem der geplante Workshop mit Mitarbeiter:innen der Berliner Verwaltung aufgrund von Corona kurzfristig abgesagt werden musste, arbeiten wir nun fleißig daran, den Workshop auf digital umzustellen.

<!--more-->

Eine besondere Herausforderung besteht darin, dass das Workshop-Konzept sehr kollaborativ ausgelegt ist. Die direkte Zusammenarbeit zwischen den Teilnehmer:innen bildet eine wichtige Grundlage für den kreativen Prozess und ist Vorausetzung, um die vielfältigen Perspektiven der Mitarbeiter:innen einzufangen. Entsprechend versuchen wir, die virtuellen Möglichkeiten möglichst gut auszuschöpfen, bspw. durch die Bildung digitaler Kleingruppen oder durch den Einsatz kollaborativer Tools. Derzeit probieren wir verschiedene Möglichkeiten aus, um euch einen abwechslungsreichen und spannenden Mitmach-Workshop anbieten zu können, der die Teilnehmer:innen gleichzeitig nicht überfordert.

Den neuen Workshop-Termin werden wir Euch bald bekannt geben. Sobald die Anmeldung für den Workshop möglich ist, werden wir hier im Blog darüber berichten.
