+++
fragment = "content"
weight = 100
date = "2021-04-16"

title = "KiezRadar ist Open Source"
title_align = "left"
+++

Endlich.

<https://gitlab.com/kiezradar/>

<!--more-->

[Vor einem Monat](../2021-03-19-open-source/) hatten wir alles geklärt und mit der Webseite angefangen, KiezRadar Open Source zu stellen.

Jetzt sind der Quellcode der App und des Backends dazugekommen.

Die Quellen liegen unter:

- <https://gitlab.com/kiezradar/app>
- <https://gitlab.com/kiezradar/backend>
- <https://gitlab.com/kiezradar/website>

Jeder Bereich hat seinen eigenen Issue-Tracker, wie bei gitlab üblich, forken ist erlaubt, wir nehmen auch Merge-Requests an.

Backend und App besitzen auch eine erste Dokumentation

- <https://kiezradar.gitlab.io/app/>
- <https://kiezradar.gitlab.io/backend/>

Viel Spaß mit dem Code...
