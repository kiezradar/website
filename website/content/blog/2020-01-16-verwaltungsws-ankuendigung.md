+++
fragment = "content"
weight = 100
date = "2020-02-10"

title = "KiezRadar-Workshop mit Mitarbeiter*Innen der Berliner Verwaltung"
title_align = "left"
+++

Berlin gehört zu den wenigen Städten, die mit meinBerlin.de eine eigene Plattform zur Bündelung stadteigener Beteiligungsvorhaben anbieten. Künftig soll die KiezRadar-App, die der Geschäftsbereich Digital Public Services des Fraunhofer-Instituts FOKUS derzeit entwickelt, das vorhandene Informationsangebot ergänzen und Bürger*Innen noch zielgerichteter über Beteiligungsmöglichkeiten informieren.

<!--more-->

Der Workshop im CityLAB Berlin bietet die Möglichkeit, dass Mitarbeiter*Innen der Berliner Verwaltung Ihre Gedanken, Wünsche, Ziele und Bedenken zu der späteren KiezRadar-App diskutieren und Ideen zu möglichen Anwendungsszenarien oder Eigenschaften der App entwickeln. Gemeinsam wollen wir zudem die Umsetzbarkeit der Ergebnisse und Ansätze aus dem Bürgerworkshop diskutieren.

Mit dem Workshop verfolgen wir folgende Ziele:

- Den bisherigen Informationsprozess zu Beteiligungsvorhaben, zu aktuellen Informationen aus dem Abgeordnetenhaus und den Bezirksverordnetenversammlungen verstehen.
- Nutzerzentrierung der späteren KiezRadar-App fördern.
- Best Practice-Ansatz für spätere Verwaltungsprojekte in Berlin schaffen.


### Daten

Datum: 12.03.2020\
Dauer: 4 Stunden, inklusive Pausen (10:00-14:00 Uhr)\
Ort: CityLAB Berlin (Platz der Luftbrücke 4)\
Voraussetzungen: Keine\
Anmeldefrist: 10.03.2020

Hier gehts zur Anmeldung: https://www.fokus.fraunhofer.de/de/dps/kiezradar
