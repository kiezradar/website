+++
fragment = "content"
weight = 100
date = "2020-05-27"

title = "Neues Mockup der Event-Detailansicht der App"
title_align = "left"
[asset]
	image = "blog/2020-05-27-Mockup-Detail-small.png"
+++

Um Euch unsere Konzepte für neue Inhalte der App zu präsentieren haben wir in dieser Woche ein weiteres Mockup für die Event-Detailansicht erstellt.

<!--more-->

In der Detailansicht können verschiedene Interaktionen mit dem Projekt getätigt werden. So kann bspw. direkt auf "beteiligen" geklickt oder das Event geliked, als Favorit markiert sowie im eigenen Kalender gespeichert werden.

Um die Funktionalitäten der App auf Eure Bedürfnisse zu zuschneiden sind wir auf Feedback von Euch angewiesen. Im Falle der Detailansicht stellt sich uns die Frage, ob beispielsweise der Ort der Beteiligung in einer separaten Kartenansicht angezeigt oder direkt in der Kartenfunktion der KiezRadar-App geöffnet werden soll? Falls ihr Anregungen habt und gerne an der Verbesserung der App mitwirken wollt, zögert nicht, uns eine Mail zu schreiben.

<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#105;&#101;&#122;&#114;&#97;&#100;&#97;&#114;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;" class="btn btn-primary"><span class="far fa-envelope" title="E-Mail"></span> Mail an uns...</a>

<figure class="figure">
  <a href="/images/blog/2020-05-27-Mockup-Detail.png"><img src="/images/blog/2020-05-27-Mockup-Detail.png" class="figure-img img-fluid rounded" alt="Screenshot: Map-Implementierung der App" width="50%"></a>
  <figcaption class="figure-caption">Mockup der Event-Detailansicht</figcaption>
</figure>
