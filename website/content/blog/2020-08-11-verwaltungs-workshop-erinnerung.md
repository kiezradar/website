+++
fragment = "content"
weight = 100
date = "2020-08-11"

title = "Erinnerung: KiezRadar-Workshop im September"
title_align = "left"
+++

Noch einmal zur Erinnerung: Der Termin für den virtuellen KiezRadar-Workshop ist der **10. September 2020, von 14.00-17.00 Uhr** Wir laden Mitarbeiter:innen der Berliner Verwaltung zum virtuellen Köpferauchen, diskutieren und Ideen schmieden ein.

<!--more-->

Wir haben uns einige Methode und Aufgaben überlegt, um die Workshopatmosphäre digital erlebbar zu machen. Um die Teilnehmer:innen nicht zu überfordern, haben wir Pausen vorgesehen und den Workshop um eine Stunde gekürzt.

Anmelden könnte Ihr euch auf der FOKUS-Hompage unter: https://www.fokus.fraunhofer.de/de/dps/events/kiezradar-workshop-verwaltung

Wir freuen uns auf spannende Einblicke und Ideen.
