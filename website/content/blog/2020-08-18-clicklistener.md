+++
fragment = "content"
weight = 100
date = "2020-08-18"

title = "Nutzung eines ClickListeners für Items und Buttons in einem ListView-Kontext"
title_align = "left"
+++

Möchte man Daten in einer Listenansicht anzeigen nutzt man in der Regel die Klasse *ListView*, ein Layout für ein einzelnes Elemente und erstellt dann einen *ArrayAdapter*, der dann die gewünschten Daten mit dem *ViewView* verbindet.

<!--more-->

Im Detail kann man das hier <https://www.programmierenlernenhq.de/tutorial-android-listview-verwenden/> an einem Beispiel sehen, sollte sich jedoch aber auch nochmal die Dokumentation anschauen: <https://developer.android.com/reference/android/widget/ListView>.

Für uns ist zudem wichtig, dass wir einen *ItemClickListener* implementieren, damit bei dem Klick auf das jeweilige Element der Liste, die Detailsansicht des jeweiligen Elements geöffnet werden kann.

Das kann man zum Beispiel vereinfacht so implementieren:

```kotlin
listView.setOnItemClickListener { parent , view, position, id ->
	// use companion object to transfer data
	var selectedEventInEventDetails = arrayListEventDetails[position]

	// load eventdetailsfragment
	loadSubFragment(activity?.supportFragmentManager!!, EventDetailsFragment())
}
```

Der zu öffnende Datensatz für die Detailansicht kann natürlich auch in ein *ViewModel* geschrieben werden.

Möchte man nun in dem Layout für das einzelne Element einen oder mehrere Button platzieren ergibt sich ein Konflikt!

Durch das Einfügen eines Buttons, der als ein klickbares Objekt interpretiert wird, kann der *ItemClickListener* für das jeweilige Element der Liste nicht mehr verwendet werden bzw. die Funktion ist außer Kraft gesetzt.

Um dieses Problem zu lösen, gibt es drei einfache Schritte:

1. Setze in dem Eltern-Objekt im Layout für ein einzelnes Element die Eigenschaft:

```kotlin
android:descendantFocusability="blocksDescendants"
```

2. Setze in dem jeweiligen Kind-Objekt (Button) im Layout für ein einzelnes Element die Eigenschaft:

```kotlin
android:focusable="false"
android:focusableInTouchMode="false"
```


Mit den Schritten 1. und 2. sind die klickbaren Objekte (Buttons) nicht mehr fokussierbar und damit zunächst auch nicht mehr klickbar.
Demnach entsteht erstmal kein Konflikt zu unserem *ItemClickListener*.

Hierbei ist jedoch zu beachten, dass die Flächen der klickbaren Objekte nicht mehr funktionieren, um die Detailansicht eines Elements der Liste zu öffnen.

Diese Objekte z.B. Buttons können jedoch jetzt noch mit einer Eigenschaft im Layout versehen werden, die dafür sorgt, dass eine gewünschte Funktion ausgeführt wird.

3. Setze die Eigenschaft, die eine bestimmte Funktion (in diesem Fall 'sendMessage') ausführt:

```kotlin
android:onClick="sendMessage"
```
