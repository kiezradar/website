+++
fragment = "content"
weight = 100
date = "2021-01-13"

title = "Neues Filterdesign für die KiezRadar-App"
title_align = "left"
+++

Die Nutzertests haben Schwachstellen des Filterdesigns der KiezRadar-App offen gelegt.
Daraufhin haben wir das Filterdesign überarbeitet mit dem Ziel einer funktionalen und übersichtlichen Anordnung der Elemente.

<!--more-->

## Übersichtlichkeit

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-ueber-vorher.png"><img src="/images/blog/2021-01-13-ueber-vorher.png" alt="Übersichtlichkeit (vorher)." title="Übersichtlichkeit (vorher)" class="card-img" /></a>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title">vorher</h4>
				<p class="card-text">
					Alle Filtermöglichkeiten mit ihren Einstellungen werden angezeigt, sobald sobald die Filterfunktion geöffnet wird.
					Es muss gescrollt werden, um Filterparameter zu sehen, die sich weiter unten befinden.
					Eingestellte Filterparameter werden als Kacheln unter "Ausgewählt" angezeigt.
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title text-right">nachher</h4>
				<p class="card-text text-right">
					Beim Öffnen der Filterfunktion erscheint eine Übersicht mit allen fünf Filterparametern.
					Nutzer:innen sehen auf einen Blick, welche Filterparameter es gibt, sind jedoch nicht mit großen Menge von Informationen konfrontiert.
					Der gewünschte Parameter kann durch Klicken auf den Pfeil ausgeklappt und eingestellt werden.
					Eingestellte Filterparameter werden weiterhin als Kacheln unter "Ausgewählt" angezeigt.
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-ueber-nachher.png"><img src="/images/blog/2021-01-13-ueber-nachher.png" alt="Übersichtlichkeit (nachher)." title="Übersichtlichkeit (nachher)" class="card-img" /></a>
		</div>
	</div>
</div>

## Funktionalität

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-funkt-vorher.png"><img src="/images/blog/2021-01-13-funkt-vorher.png" alt="Funktionalität (vorher)." title="Funktionalität (vorher)" class="card-img" /></a>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title">vorher</h4>
				<p class="card-text">
					Die Parameter "Thema" und "Ereignistyp" können nur über Texteingabe gefunden werden.
					Es ist intransparent für Nutzer:innen, welche Einstellungsmöglichkeiten es für diese Parameter gibt, da sie nirgends aufgelistet sind.
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title text-right">nachher</h4>
				<p class="card-text text-right">
					Das Setzen von Filterparametern durch eine direkte Texteingabe bleibt erhalten.
					Zudem werden alle Filterparameter durch Ausklappen in der Übersicht sichtbar mit den jeweiligen Einstellungsmöglichkeiten.
					Für die Parameter "Thema" und "Ereignistyp" können Häkchen neben die jeweiligen Einträge gesetzt werden, die als Filtereinstellung übernommen werden sollen.
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-funkt-nachher.png"><img src="/images/blog/2021-01-13-funkt-nachher.png" alt="Funktionalität (nachher)." title="Funktionalität (nachher)" class="card-img" /></a>
		</div>
	</div>
</div>

## Ansprechendes Design

<div class="card mb-3">
	<div class="row no-gutters">
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-design-vorher.png"><img src="/images/blog/2021-01-13-design-vorher.png" alt="Ansprechendes Design (vorher)." title="Ansprechendes Design (vorher)" class="card-img" /></a>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title">vorher</h4>
				<p class="card-text">
					Die Filterfunktion besteht vor Allem aus textueller Information und stellt eine Fülle von Informationen auf einer Seite dar.
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card-body">
				<h4 class="card-title text-right">nachher</h4>
				<p class="card-text text-right">
					An sinnvollen Stellen wird textuelle Information visuell unterstützt.
					Die Informationen sind übersichtlich durch Ein- und Ausklappen gegliedert.
				</p>
			</div>
		</div>
		<div class="col-md-2 p-2">
			<a href="/images/blog/2021-01-13-design-nachher.png"><img src="/images/blog/2021-01-13-design-nachher.png" alt="Ansprechendes Design (nachher)." title="Ansprechendes Design (nachher)" class="card-img" /></a>
		</div>
	</div>
</div>
