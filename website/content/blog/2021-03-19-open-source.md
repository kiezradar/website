+++
fragment = "content"
weight = 100
date = "2021-03-19"

title = "Open Source nimmt Gestalt an"
title_align = "left"
+++

KiezRadar wird Open Source.
Nicht mehr nur auf dem Papier, sondern auf [gitlab](https://gitlab.com/kiezradar)

<!--more-->

Das hat etwas gedauert, denn es waren viele rechtliche Fragen zu klären, aber jetzt geht es los.

Die [Webseite](https://gitlab.com/kiezradar/website) war das erste, das auf gitlab erschien, es folgte die [App](https://gitlab.com/kiezradar/app) (erst einmal nur APKs) und seit heute gibt es das [Backend](https://gitlab.com/kiezradar/backend).

Dabei ist das Aufsetzen eine Aufgabe, die gerade am Anfang viel Zeit und Mühe bereitet.
Es muss das Repository angelegt werden, dann wird der "master"-Branch in "main" umbenannt.
Die Repository-Einstellungen werden angepasst und die gitlab-Features ausgewählt, die eingesetzt werden.

Jetzt muss der [Issue-Tracker](https://gitlab.com/kiezradar/backend/-/issues) mit Meilensteinen und Labels befüllt werden, damit Tickets ordentlich bearbeitet werden können.

Die [Dokumentation](https://kiezradar.gitlab.io/backend/) soll ebenfalls in gitlab erfolgen, dafür nutzen wir [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) und gitlab pages, auch das muss erst einmal initialisiert und per [gitlab-CI](https://gitlab.com/kiezradar/backend/-/blob/main/.gitlab-ci.yml) verbunden werden.

Jetzt müssen wir einen Workflow für das Repository festlegen und die bereits bestehenden Inhalte übernehmen.

Dabei zeigt sich wie erwartet, dass es bei Open-Source-Projekten besser ist, von Anfang an zu veröffentlichen, damit man Dokumentation und Code gleichzeitig erstellt und nicht alles nachdokumentieren muss.
Leider gab es da die rechtliche Verzögerung...
Natürlich hätten wir auch gleich "richtig" dokumentieren können, aber ein internes Repository verführt dazu, das auf später zu verschieben.

Außerdem müssen manche Workflows erst etabliert werden, so in unserem Projekt z.B. die Dokumentation mit MkDocs, die wir erst seit kurzem führen.

Aber das ist der Vorteil an agilen Projekten - alles kann sich einspielen und am Ende gewinnt meist die beste Lösung.

Der vollständige Quellcode von Backend und App wird bis Ende März online sein.
Mitte April gibt es auch die Abschlussdokumentation des Projekts auf der Webseite.
