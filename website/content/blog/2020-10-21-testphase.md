+++
fragment = "content"
weight = 100
date = "2020-10-21"

title = "Erinnerung: KiezRadar-Testphase"
title_align = "left"
+++

Was passiert in meinem Kiez?
Welche Beteiligungsmöglichkeiten gibt es in meiner Umgebung?
Womit befasst sich die Lokalpolitik?

<!--more-->

Die KiezRadar-App soll zukünftig über wichtige Ereignisse aus Politik und Verwaltung informieren.
Dabei nutzt die KiezRadar-App Daten vorhandener Informationsangebote wie bspw. mein.berlin.de oder die der Ratsinformationssysteme der Bezirke und konsolidiert diese in einer einzigen Lösung.

Nach Workshops mit Bürger:innen und der Berliner Verwaltung haben wir fleißig daran gearbeitet, ihre Ideen in einen Prototypen der App einzuarbeiten.
Die Evaluation im Rahmen der Science Week über die wir bereits letzte Woche berichtet haben, bildet den Auftakt für die Erprobung und Verbesserung des Prototyps.

Neben den Nutzertests während der [Science Week](/science-week) wird der Protoype für mehrere Wochen im [CityLAB Berlin](https://www.citylab-berlin.org/) ausgestellt.
Hier kann der Prototype auf dem Tablet getestet werden.

Der Funktionsumfang der Applikation ermöglicht es dabei durch verschiedene Filtereinstellungen, passende Beteiligungsverfahren und Angebote aus dem Kiez aufzufinden und zu verfolgen.
Im Rahmen dieser Testphase erheben wir mithilfe eines weiteren Tablets zudem Nutzerfeedback, um den Prototypen sukzessiv verbessern zu können.
Dabei ist das Ziel eine möglichst intuitive App zu schaffen, die denen hilft, die sie im Einsatz haben - nämlich den Bürgerinnen und Bürgern.

<figure class="figure">
  <a href="/images/blog/2020-09-23-Startseite.png"><img src="/images/blog/2020-09-23-Startseite.png" class="figure-img img-fluid rounded" alt="Mockup Startseite." width="50%"></a>
  <figcaption class="figure-caption">Mockup Startseite.</figcaption>
</figure>
