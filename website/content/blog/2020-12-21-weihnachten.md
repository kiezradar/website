+++
fragment = "content"
weight = 100
date = "2020-12-21"

title = "Schöne Feiertage und einen guten Rutsch"
title_align = "left"

[asset]
	image = "blog/2020-12-21-Weihnachtslogo-small.png"
+++

Wünscht das KiezRadar-Team.

<!--more-->

<figure class="figure">
	<a href="/images/blog/2020-12-21-Weihnachtslogo.png"><img src="/images/blog/2020-12-21-Weihnachtslogo.png" class="figure-img img-fluid rounded" alt="KiezRadar-Logo mit Weihnachtsmütze." width="40%"></a>
</figure>
