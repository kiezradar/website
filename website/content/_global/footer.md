+++
fragment = "footer"
weight = 1200

[[assets]]
  title = "Fraunhofer FOKUS"
  image = "fokus.png"
  url = "https://www.fokus.fraunhofer.de/"

[[assets]]
  title = "CityLAB Berlin"
  image = "citylab-logo_rgb.png"
  url = "https://www.citylab-berlin.org/"

[[assets]]
  title = "Senatskanzlei Berlin"
  image = "SKzl_flach_rgb.png"
  url = "https://www.berlin.de/rbmskzl/"
+++

<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link p-0 display-5" href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#105;&#101;&#122;&#114;&#97;&#100;&#97;&#114;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;"><span class="far fa-envelope" title="E-Mail"></span> E-Mail</a>
  </li>
  <li class="nav-item">
    <a class="nav-link p-0 display-5" href="/blog/feed.xml"><span class="fa fa-rss" title="RSS-Feed"></span> RSS-Feed</a>
  </li>
</ul>
