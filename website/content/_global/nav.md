+++
fragment = "nav"
weight = 0

# [repo_button]
#   url = "https://gitlab.fokus.fraunhofer.de/senatskanzleiberlin/Kiezradar"
#   text = "GitLab" # default: "GitLab"
#   icon = "fab fa-gitlab" # defaults: "fab fa-gitlab"

# Branding options
[asset]
  image = "navlogo.png"
  text = "KiezRadar"

[[postpend]]
	name = "RSS-Feed"
	icon = "fa fa-rss"
	url = "/blog/feed.xml"
+++
