+++
fragment = "content"
date = "2020-04-09"
weight = 100

title = "Datenschutzinformation"
+++

8\. April 2020

Im Rahmen der Nutzung dieser Webseite werden personenbezogene Daten von Ihnen durch uns als den für die Datenverarbeitung Verantwortlichen verarbeitet und für die Dauer gespeichert, die zur Erfüllung der festgelegten Zwecke und gesetzlicher Verpflichtungen erforderlich ist.
Im Folgenden informieren wir Sie darüber, um welche Daten es sich dabei handelt, auf welche Weise sie verarbeitet werden und welche Rechte Ihnen diesbezüglich zustehen.

Personenbezogene Daten sind gemäß Art.4 Nr. 1 Datenschutzgrundverordnung (DSGVO) alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen.

### 1. Name und Kontaktdaten des für die Verarbeitung Verantwortlichen sowie des betrieblichen Datenschutzbeauftragten

Diese Datenschutzinformation gilt für die Datenverarbeitung auf unserer Webseite <https://www.kiezradar.de/>  durch den Verantwortlichen:

Fraunhofer-Gesellschaft\
zur Förderung der angewandten Forschung e.V.\
Hansastraße 27 c,\
80686 München

für das Fraunhofer Institut für Offene Kommunikationssysteme\
E-Mail: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a>

Der Datenschutzbeauftragte von Fraunhofer ist unter der o.g. Anschrift, zu Hd. Datenschutzbeauftragter bzw. unter <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#97;&#116;&#101;&#110;&#115;&#99;&#104;&#117;&#116;&#122;&#64;&#122;&#118;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#100;&#97;&#116;&#101;&#110;&#115;&#99;&#104;&#117;&#116;&#122;&#64;&#122;&#118;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a> erreichbar.

Sie können sich jederzeit bei Fragen zum Datenschutzrecht oder Ihren Betroffenenrechten direkt an unseren Datenschutzbeauftragten wenden.

### 2. Verarbeitung personenbezogener Daten und Zwecke der Verarbeitung

#### a) Beim Besuch der Webseite

Wenn Sie unsere Webseiten besuchen, speichern die Webserver unserer Webseite temporär jeden Zugriff Ihres Endgerätes in einer Protokolldatei.
Folgende Daten werden erfasst und bis zur automatisierten Löschung gespeichert:

- IP-Adresse des anfragenden Rechners
- Datum und Uhrzeit des Zugriffs
- Name und URL der abgerufenen Daten
- Übertragene Datenmenge

Meldung, ob der Abruf erfolgreich war

- Verwendeter Browser und Browser-Einstellungen
- Betriebssystem
- Name des Internet-Zugangs-Providers
- Webseite, von der aus der Zugriff erfolgt (Referrer-URL)

Die Verarbeitung dieser Daten erfolgt zu folgenden Zwecken:

1.	Ermöglichung der Nutzung der Webseite (Verbindungsaufbau)
2.	Administration der Netzinfrastruktur
3.	Angemessene technisch-organisatorische Maßnahmen zur IT-System und Informationssicherheit unter Berücksichtigung des Stands der Technik
4.	Gewährleistung der Nutzerfreundlichkeit der Nutzung
5.	Optimierung des Internetangebotes

Rechtsgrundlagen für die vorstehenden Verarbeitungen sind

- für die Verarbeitung für den Besuch der Webseiten nach den Nummern 1-2 Art. 6 Abs. 1 S.1 lit. b DSGVO (Erforderlichkeit für die Erfüllung des Webseiten-Nutzungsvertragsverhältnisses),
- für die Verarbeitungen nach Nummer 3 Art. 6 Abs. 1 S.1 lit. c DSGVO (rechtliche Verpflichtung zur Umsetzung technisch-organisatorischer Maßnahmen zur Sicherung der Datenverarbeitung nach Art.
32 DSGVO) und Art. 6 Abs. 1 S.1 lit. f DSGVO (berechtigte Interessen zur Datenverarbeitung für die Netz- und Informationssicherheit) sowie für
- die Verarbeitungen nach den Nummern 4-5 Art. 6 Abs. 1 S.1 lit. f DSGVO (berechtigte Interessen).

Die berechtigten Interessen unserer Datenverarbeitung bestehen darin, unser Angebot nutzerfreundlich zu gestalten und zu optimieren.

- Die vorstehend genannten Daten werden nach einer definierten Zeit vom Webserver automatisiert gelöscht, die 30 Tage beträgt.
	Sofern Daten zu Zwecken nach Nummern 2-5 länger verarbeitet werden, erfolgt die Anonymisierung oder Löschung, wenn die Speicherung für den jeweiligen Zweck nicht mehr erforderlich ist.

Darüber hinaus setzen wir beim Besuch unserer Website Cookies sowie Analysedienste ein.
Nähere Erläuterungen dazu erhalten Sie unter den Ziffer 4 und 5  dieser Datenschutzinformation.

### 3. Weitergabe von personenbezogenen Daten

Außer in den zuvor genannten Fällen einer Verarbeitung im Auftrag (Anmeldung für Veranstaltungen (Zusatz Datenschutzerklärung auf Veranstaltungsseite), Anmeldung zu einem Newsletter, Kontaktformular und Bestellformular) geben wir Ihre personenbezogenen Daten nur an Dritte, d.h. andere natürliche oder juristische Personen außer Ihnen (der betroffenen Person), dem Verantwortlichen oder dem Auftragsverarbeiter und deren zur Datenverarbeitung befugten Mitarbeiter weiter, wenn:

- Sie gem. Art. 6 Abs. 1 S. 1 lit. a DSGVO Ihre ausdrückliche Einwilligung dazu erteilt haben;
- dies gem. Art. 6 Abs. 1 S. 1 lit. b DSGVO für die Erfüllung eines Vertrages mit Ihnen erforderlich ist,
  - Weitergabe an Versandunternehmen zum Zwecke der Lieferung der von Ihnen bestellten Ware,
  - Weitergabe von Zahlungsdaten an Zahlungsdienstleister bzw. Kreditinstitute, um einen Zahlungsvorgang durchzuführen;
- für den Fall, dass für die Weitergabe nach Art. 6 Abs. 1 S. 1 lit. c DSGVO eine gesetzliche Verpflichtung besteht, bspw. an Finanz- oder Strafverfolgungsbehörden;
- die Weitergabe ist nach Art. 6 Abs. 1 S. 1 lit. f DSGVO zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich und es besteht kein Grund zur Annahme, dass Sie ein überwiegendes schutzwürdiges Interesse an der Nichtweitergabe Ihrer Daten haben; eine solche Weitergabe kann bspw. im Falle von Angriffen auf unsere IT-Systeme an staatliche Einrichtungen und Strafverfolgungsbehörden erfolgen.

Die weitergegebenen Daten dürfen von dem Dritten ausschließlich zu den genannten Zwecken verwendet werden.

Wenn Sie sich für eine Veranstaltung angemeldet haben, kann es im Rahmen der Vertragserfüllung erforderlich sein, dass Ihre personenbezogenen Daten an einen externen Veranstalter übermittelt werden müssen.
Im Zusammenhang mit einer Veranstaltungsanmeldung werden Sie darüber informiert, wer Veranstalter ist und ob es sich dabei um einen externen Veranstalter handelt.
Dieser wird personenbezogenen Daten im Rahmen der Veranstaltung und insbesondere zur Teilnehmerverwaltung verarbeiten.

Im Übrigen werden unsere Webseiten gemäß Art. 28 DSGVO von unserem Auftragsverarbeiter Infopark AG, Kitzingstraße 15, 12277 Berlin, Deutschland ausschließlich auf Servern in Deutschland gehostet.

Eine Übermittlung von personenbezogenen Daten an ein Drittland (außerhalb der EU) oder eine internationale Organisation ist ausgeschlossen.

### 4. Betroffenenrechte

Sie haben das Recht:
- gemäß Art. 7 Abs. 3 DSGVO Ihre einmal erteilte Einwilligung jederzeit gegenüber uns zu widerrufen.
	Dies hat zur Folge, dass wir die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen dürfen;
- gemäß Art. 15 DSGVO Auskunft über Ihre von uns verarbeiteten personenbezogenen Daten zu verlangen.
	Insbesondere können Sie Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft ihrer Daten, sofern diese nicht bei uns erhoben wurden, sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen;
- gemäß Art. 16 DSGVO unverzüglich die Berichtigung unrichtiger oder Vervollständigung Ihrer bei uns gespeicherten personenbezogenen Daten zu verlangen;
- gemäß Art. 17 DSGVO die Löschung Ihrer bei uns gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist;
- gemäß Art. 18 DSGVO die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von Ihnen bestritten wird, die Verarbeitung unrechtmäßig ist, Sie aber deren Löschung ablehnen und wir die Daten nicht mehr benötigen, Sie jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß Art. 21 DSGVO Widerspruch gegen die Verarbeitung eingelegt haben;
- gemäß Art. 20 DSGVO Ihre personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesebaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen und
- gemäß Art. 77 DSGVO sich bei einer Aufsichtsbehörde zu beschweren.
	In der Regel können Sie sich hierfür an die Aufsichtsbehörde ihres üblichen Aufenthaltsortes oder Arbeitsplatzes oder unseres Unternehmenssitzes wenden.

<div class="card mb-4">
	<div class="card-header bg-primary text-light">
		<h5 class="card-title">Information über Ihr Widerspruchsrecht nach Art. 21 DSGVO</h5>
	</div>
  <div class="card-body bg-info text-light">
    <p class="card-text">
			Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund von Artikel 6 Absatz 1 Buchstabe e DSGVO (Datenverarbeitung im öffentlichen Interesse) und Artikel 6 Absatz 1 Buchstabe f DSGVO (Datenverarbeitung auf der Grundlage einer Interessenabwägung) erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmung gestütztes Profiling von Artikel 4 Nr. 4 DSGVO.
		</p>
		<p class="card-text">
			Legen Sie Widerspruch ein, werden wir Ihre personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
		</p>
		<p class="card-text">
			Sofern sich Ihr Widerspruch gegen eine Verarbeitung von Daten zum Zwecke der Direktwerbung richtet, so werden wir die Verarbeitung umgehend einstellen.
			In diesem Fall ist die Angabe einer besonderen Situation nicht erforderlich.
			Dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht.
		</p>
	</div>
	<div class="card-footer bg-primary text-light">
		<p class="card-text">
			Möchten Sie von Ihrem Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#97;&#116;&#101;&#110;&#115;&#99;&#104;&#117;&#116;&#122;&#64;&#122;&#118;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#100;&#97;&#116;&#101;&#110;&#115;&#99;&#104;&#117;&#116;&#122;&#64;&#122;&#118;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a>.
		</p>
  </div>
</div>

### 5. Aktualität und Änderung dieser Datenschutzinformation

Diese Datenschutzinformation ist aktuell gültig und hat den Stand April 2020.

Durch die Weiterentwicklung unserer Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher bzw. behördlicher Vorgaben kann es notwendig werden, diese Datenschutzinformation zu ändern.
Die jeweils aktuelle Datenschutzinformation kann jederzeit auf der Webseite unter Link auf diese Seite: <https://www.kiezradar.de/datenschutz/>  von Ihnen abgerufen und ausgedruckt werden.


### 6. Salvatorische Klausel

Sollten einzelne Bestimmungen dieser Datenschutzerklärung ganz oder in Teilen unwirksam oder undurchführbar sein oder werden, berührt dies nicht die Wirksamkeit der übrigen Bestimmungen.
Entsprechendes gilt im Fall von Lücken.
