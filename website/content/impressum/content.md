+++
fragment = "content"
date = "2024-03-26"
weight = 100

title = "Impressum"
+++

Das Fraunhofer-Institut für Offene Kommunikationssysteme FOKUS\
Kaiserin-Augusta-Allee 31\
10589 Berlin\
Tel: +49 30 3463-7000

<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a>

ist eine rechtlich nicht selbständige Einrichtung der

Fraunhofer-Gesellschaft\
zur Förderung der angewandten Forschung e.V.\
Hansastraße 27 c\
80686 München

<https://www.fraunhofer.de/>

Umsatzsteuer-Identifikationsnummer gemäß § 27 a\
Umsatzsteuergesetz: DE 129515865

**Registergericht**\
Amtsgericht München\
Eingetragener Verein\
Register-Nr. VR 4461

**Verantwortlicher Redakteur**\
Ekkart Kleinod\
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#101;&#107;&#107;&#97;&#114;&#116;&#46;&#107;&#108;&#101;&#105;&#110;&#111;&#100;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#101;&#107;&#107;&#97;&#114;&#116;&#46;&#107;&#108;&#101;&#105;&#110;&#111;&#100;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a>

---

*Hinweis: Bei allgemeinen Fragen zum Institut, wenden Sie sich bitte an die unter [Kontakt Fraunhofer FOKUS](https://www.fokus.fraunhofer.de/kontakt) angegebenen Ansprechpartner*

### Vorstand

Prof. Dr.-Ing. Holger Hanselka | Präsident\
Prof. Dr. Axel Müller-Groeling | Mitglied des Vorstands\
Ass. jur. Elisabeth Ewen | Mitglied des Vorstands\
Dr. Sandra Krey | Mitglied des Vorstands

### Nutzungsrechte

Copyright © by\
Fraunhofer-Gesellschaft

Alle Rechte vorbehalten.

Die Urheberrechte dieser Web-Site liegen vollständig bei der Fraunhofer-Gesellschaft.

Ein Download oder Ausdruck dieser Veröffentlichungen ist ausschließlich für den persönlichen Gebrauch gestattet.
Alle darüber hinaus gehenden Verwendungen, insbesondere die kommerzielle Nutzung und Verbreitung, sind grundsätzlich nicht gestattet und bedürfen der schriftlichen Genehmigung.
Anfragen richten Sie Bitte an folgende Adresse:

Das Fraunhofer-Institut für Offene Kommunikationssysteme FOKUS\
Kaiserin-Augusta-Allee 31\
10589 Berlin\
Tel: +49 30 3463-7000\
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;">&#105;&#110;&#102;&#111;&#64;&#102;&#111;&#107;&#117;&#115;&#46;&#102;&#114;&#97;&#117;&#110;&#104;&#111;&#102;&#101;&#114;&#46;&#100;&#101;</a>

Ein Download oder Ausdruck ist darüber hinaus lediglich zum Zweck der Berichterstattung über die Fraunhofer-Gesellschaft und Ihrer Institute nach Maßgabe unten stehender Nutzungsbedingungen gestattet:

Grafische Veränderungen an Bildmotiven – außer zum Freistellen des Hauptmotivs – sind nicht gestattet.
Es ist stets die Quellenangabe und Übersendung von zwei kostenlosen Belegexemplaren an die oben genannte Adresse erforderlich.
Die Verwendung ist honorarfrei.

### Haftungshinweis

Wir übernehmen keine Haftung für die Inhalte externer Links.
Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.

Wir sind bemüht, das Webangebot stets aktuell und inhaltlich richtig sowie vollständig anzubieten.
Dennoch ist das Auftreten von Fehlern nicht völlig auszuschließen.
Das Fraunhofer-Institut bzw. die Fraunhofer-Gesellschaft übernimmt keine Haftung für die Aktualität, die inhaltliche Richtigkeit sowie für die Vollständigkeit der in ihrem Webangebot eingestellten Informationen.
Dies bezieht sich auf eventuelle Schäden materieller oder ideeller Art Dritter, die durch die Nutzung dieses Webangebotes verursacht wurden.

Geschützte Marken und Namen, Bilder und Texte werden auf unseren Seiten in der Regel nicht als solche kenntlich gemacht.
Das Fehlen einer solchen Kennzeichnung bedeutet jedoch nicht, dass es sich um einen freien Namen, ein freies Bild oder einen freien Text im Sinne des Markenzeichenrechts handelt.
