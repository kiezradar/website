+++
fragment = "hero"
#disabled = true
weight = 10
particles = false

title = "KiezRadar"
subtitle = "Die App für mehr Bürgerbeteiligung"

#[header]
#  image = "header.jpg"

[asset]
  image = "header_logo.png"

[[buttons]]
  text = "Neues im Blog"
  url = "#latestentries"
  color = "info"

[[buttons]]
  text = "Kleine Vorschau"
  url = "#items"
  color = "info"

[[buttons]]
  text = "Das Team"
  url = "#team"
  color = "info"

[[buttons]]
  text = "Das Projekt"
  url = "/project"
  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

	[[buttons]]
	  text = "Science Week"
	  url = "/science-week"
	  color = "success" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

+++
