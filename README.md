# KiezRadar - website

This repository contains the website of KiezRadar:

https://kiezradar.fokus.fraunhofer.de/

The website is created with:

- [hugo](https://gohugo.io)
- [syna theme for hugo](https://about.okkur.org/syna/)

There were three main reasons for choosing a static website generator:

1. no user input, meaning less risk for violation of data handling of input data
2. well, static websites: smaller pages, less attack vectors (see [benefits](https://gohugo.io/about/benefits/) for more arguments)
3. automated deployment (CD) of the generated website after changes

Unfortunately, due to security measures of the web server the CD is rather difficult and is not implemented yet, but the other reasons are still valid.

There are two main drawbacks of the approach (in case you consider using a static website generator yourself):

1. installing hugo, git, ... in order to change websites yourself
2. difficult approach for nonexperienced users when using git, hugo, text editors instead of a comfortable online WYSIWYG editor

In order to create the website yourself:

1. clone this repository
2. init and update the submodules
3. call `serve` or `build` in order to use a local server or build the static files


## Copyright

Copyright 2021-2024 KiezRadar <kiezradar@fokus.fraunhofer.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
